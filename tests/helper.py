import os, random
random.seed(os.environ["PYTHONHASHSEED"])

from uuid import uuid4

# Create a URI by hashing the public key with a multihash
from occam.storage.plugins.ipfs_vendor.multihash import multihash as vendor_multihash

from occam.storage.plugins.ipfs_vendor.base58 import b58encode

def uuid():
  """ Generates a random uuid as a string.
  """
  return str(uuid4())

def multihash(hashType="sha256"):
  """ Generates a random multihash as a string.
  """

  token = uuid()
  if hashType == "sha1":
    hashType = vendor_multihash.SHA1
  else:
    hashType = vendor_multihash.SHA2_256

  hashedBytes = vendor_multihash.encode(token, hashType)
  ret = b58encode(bytes(hashedBytes))

  return ret
