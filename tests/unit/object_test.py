import unittest

from occam.object import Object

from unittest.mock import patch


class Test__init__(unittest.TestCase):
    pass


class TestObject:
  class TestSlugFor(unittest.TestCase):
    def test_should_return_a_string(self):
      self.assertTrue(isinstance(Object.slugFor("abcd1234"), str))

    def test_should_not_contain_anything_other_than_alphanumerics_or_hyphens(self):
      import re
      self.assertIsNotNone(re.match(r"^[a-zA-Z0-9-]+$", Object.slugFor("abcd")))

    def test_should_handle_symbols(self):
      import re
      self.assertIsNotNone(re.match(r"^[a-zA-Z0-9-]+$", Object.slugFor("%^&*")))

    def test_should_yield_the_original_text(self):
      import re
      self.assertIsNotNone(re.match(r"^abcd-1234$", Object.slugFor("abcd-1234")))

  class TestSlugUUID(unittest.TestCase):
    def test_should_return_a_string(self):
      self.assertTrue(isinstance(Object.slugUUID("type", "uuid"), str))

    def test_should_not_contain_anything_other_than_alphanumerics_or_hyphens(self):
      import re
      self.assertIsNotNone(re.match(r"^[a-zA-Z0-9-]+$", Object.slugUUID("type", "uuid")))

    def test_should_handle_symbols_inside_the_given_type(self):
      import re
      self.assertIsNotNone(re.match(r"^[a-zA-Z0-9-]+$", Object.slugUUID("%^&*", "abcd")))

    def test_should_handle_symbols_inside_the_given_UUID(self):
      import re
      self.assertIsNotNone(re.match(r"^[a-zA-Z0-9-]+$", Object.slugUUID("abcd", "$%^&")))

    def test_should_begin_with_the_original_type(self):
      import re
      self.assertIsNotNone(re.match(r"^abcd", Object.slugUUID("abcd", "1234")))

    def test_should_end_with_the_original_uuid(self):
      import re
      self.assertIsNotNone(re.match(r".+1234$", Object.slugUUID("abcd", "1234")))
