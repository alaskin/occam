import unittest

from occam.notes.manager import NoteManager

from unittest.mock import patch, Mock, MagicMock, mock_open,create_autospec

class TestNoteManager:
  pass

  # class TestPath(unittest.TestCase):
  #   noteManager =  NoteManager()

  #   #Where self.configuration come from?
  #   def test_should_return_path_as_string(self):
  #     self.noteManager.configuration =  Mock()
  #     self.noteManager.configuration.get.return_value = "path/to/somewhere"
  #     self.assertIsInstance(self.noteManager.path(),str)

  # class TestPathFor(unittest.TestCase):
  #   noteManager =  NoteManager()

  #   def test_should_return_None_when_id_is_None(self):
  #     self.assertEqual(self.noteManager.pathFor(None,None),None)

  #   @patch('os.path.exists')
  #   @patch('os.mkdir')
  #   @patch('os.path.join')
  #   def test_should_return_path_to_the_note_data(self,pathJoin,mkdir,pathExists):
  #     self.noteManager.path = Mock(return_value = 'path')
  #     pathJoin.return_value = 'pathJoin'
  #     pathExists.return_value = False
  #     self.assertIsInstance(self.noteManager.pathFor([1,2,3,4,5,6,7,8],"category/test/python"),str)
  #     self.noteManager.path.assert_called()
  #     pathJoin.assert_called()
  #     pathExists.assert_called()
  #     mkdir.assert_called()

  # class TestFilePathFor(unittest.TestCase):
  #   noteManager =  NoteManager()

  #   @patch('os.path.join')
  #   def test_should_return_filename_of_the_note_data_and_revision_None(self,pathJoin):
  #     pathJoin.return_value = "filename"
  #     self.noteManager.pathFor = Mock(return_value = "pathFor")
  #     self.assertIsInstance(self.noteManager.filePathFor("id","category",None),str)
  #     pathJoin.assert_called()
  #     self.noteManager.pathFor.assert_called()

  # class TestRetrieveObjectList(unittest.TestCase):
  #   noteManager =  NoteManager()

  #   def test_should_return_map(self):
  #     self.noteManager.path = Mock(return_value = "path")
  #     self.assertIsInstance(self.noteManager.retrieveObjectList(),map)
  #     self.noteManager.path.assert_called()

  # class TestRevisionListFor(unittest.TestCase):
  #   noteManager =  NoteManager()

  #   @patch('os.listdir')
  #   def test_should_return_list(self,listdir):
  #     listdir.return_value = ["test.json"]
  #     self.noteManager.pathFor = Mock(return_value = "pathFor")      
  #     self.assertIsInstance(self.noteManager.revisionListFor("id",'category'),list)
  #     self.noteManager.pathFor.assert_called()
  #     listdir.assert_called()

  # class TestDataFor(unittest.TestCase):
  #   noteManager =  NoteManager()

  #   def test_should_return_dict(self):
  #     self.noteManager.revisionListFor = Mock(return_value = ['revisions'])
  #     self.noteManager.retrieve = Mock(return_value = "something")
  #     self.assertIsInstance(self.noteManager.dataFor("id","category"),dict)
  #     self.noteManager.revisionListFor.assert_called()
  #     self.noteManager.retrieve.assert_called()

  # class TestRetrieveValues(unittest.TestCase):
  #   noteManager =  NoteManager()

  #   def test_should_return_list(self):
  #     self.noteManager.retrieve = Mock(return_value = [{"values1":"test"}])
  #     self.assertIsInstance(self.noteManager.retrieveValues("id","category","revision"),list)
  #     self.noteManager.retrieve.assert_called()

  # class TestRetrieve(unittest.TestCase):
  #   noteManager =  NoteManager()
    
  #   @patch("builtins.open",create=True)
  #   def test_should_return_(self,p):
  #     p.return_value = mock_open(read_data="Datxa1").return_value
  #     self.noteManager.filePathFor = Mock(return_value = "something")
  #     self.noteManager.retrieve("id","category","revision",key="bar")

  # if not key in data[revision]:
  #           data[revision][key] = subItems
  #error?, data[revision][key] -> does not have key, and if it does, the execution goes to else
  # class TestMerge(unittest.TestCase):
  #   noteManager =  NoteManager()

  #   def test_should_return_dict(self):
  #     self.noteManager.dataFor = Mock(return_value = {"items":{"item1":["test"]}})
  #     self.noteManager.store = Mock()
  #     self.assertIsInstance(self.noteManager.merge('id','category',{"items":{"item1":["test1"]},"something":{"type":"int"}}),dict)
  #     self.noteManager.dataFor.assert_called()
  #     self.noteManager.store.assert_called()

  # class TestRetrieveBestRevision(unittest.TestCase):
  #   noteManager =  NoteManager()

  #   def test_should_return_None_when_len_revision_equal_0(self):
  #     self.noteManager.revisionListFor = Mock(return_value = [])
  #     self.assertEqual(self.noteManager.retrieveBestRevision("id"),None)
  #     self.noteManager.revisionListFor.assert_called()

  #   def test_should_return_first_index_of_revisions_as_string(self):
  #     self.noteManager.revisionListFor = Mock(return_value = ["test",'test2'])
  #     self.assertEqual(self.noteManager.retrieveBestRevision("id"),'test')
  #     self.noteManager.revisionListFor.assert_called()

  # class TestRetrieveBuildIds(unittest.TestCase):
  #   noteManager =  NoteManager()

  #   def test_should_return_None_when_retrieve_method_returns_empty_array(self):
  #     self.noteManager.retrieve = Mock(return_value = [])
  #     self.assertEqual(self.noteManager.retrieveBuildIds("id","revision"),None)
  #     self.noteManager.retrieve.assert_called()

  #   def test_should_return_array_when_retrieve_returns_array_with_builds(self):
  #     self.noteManager.retrieve = Mock(return_value = [{"value":{"method":"iterative"}}])
  #     self.assertIsInstance(self.noteManager.retrieveBuildIds("id","revision"),list)
  #     self.noteManager.retrieve.assert_called()

  # class TestRetrieveBuild(unittest.TestCase):
  #   noteManager =  NoteManager()

  #   #Notes does not contain retrieveBuildId would it be retrieveBuildIds?
  #   def test_should_return_None_when_retrieveBuildId_returns_None(self):
  #     self.noteManager.retrieveBuildId = Mock(return_value = None)
  #     self.assertEqual(self.noteManager.retrieveBuild("id","revision"),None)

  #   def test_should_return_None_when_retrieveBuildId_returns_(self):
  #     self.noteManager.retrieveBuildId = Mock(return_value = None)
  #     self.assertEqual(self.noteManager.retrieveBuild("id","revision"),None

  # class TestResolveOwner(unittest.TestCase):
  #   noteManager =  NoteManager()

  #   def test_should_return_owner_id(self):
  #     self.noteManager.retrieve = Mock(return_value = {"id":[{'value':"123"}]})
  #     self.assertEqual(self.noteManager.resolveOwner("id"),"123")
  #     self.noteManager.retrieve.assert_called()

  # class TestDelete(unittest.TestCase):
  #   noteManager =  NoteManager()

  #   @patch('os.path.realpath')
  #   @patch('shutil.rmtree')
  #   @patch('os.path.join')
  #   def test_method_which_does_not_have_return(self,pathjoin,rmtree,realpath):
  #     self.noteManager.pathFor = Mock(return_value = "something")
  #     pathjoin.return_value = "joinMock"
  #     realpath.return_value = "realPath"
  #     self.noteManager.delete("id")
  #     self.noteManager.pathFor.assert_called()
  #     pathjoin.assert_called()
  #     realpath.assert_called()

  # class TestResolveVersion(unittest.TestCase):
  #   noteManager =  NoteManager()

  #   def test_should_return_None_when_version_is_None(self):
  #     t = self.noteManager.resolveVersion("id",None)
  #     self.assertEqual(t[0],None)
  #     self.assertEqual(t[1],None)

  #   @patch('occam.notes.manager.Semver')
  #   def test_should_return_revision_and_currentVersion_as_String_when_currentVersion_not_None(self,semver):
  #     semver.parseVersion.return_value = "something"
  #     semver.resolveVersion.return_value = "something"
  #     self.noteManager.retrieve = Mock(return_value = {"something":[{"value":"test"}],"oneThing":"theOther"})
  #     t = self.noteManager.resolveVersion("id","version")
  #     self.assertIsInstance(t[0],str)
  #     self.assertIsInstance(t[1],str)
  #     semver.parseVersion.assert_called()
  #     semver.resolveVersion.assert_called()
  #     self.noteManager.retrieve.assert_called()

  #   @patch('occam.notes.manager.Semver')
  #   def test_should_return_revision_and_currentVersion_as_String_when_retrieve_returns_None(self,semver):
  #     semver.parseVersion.return_value = "something"
  #     semver.resolveVersion.return_value = "something"
  #     self.noteManager.retrieve = Mock(return_value = None)
  #     t = self.noteManager.resolveVersion("id","version",versions=["something"])
  #     self.assertEqual(t[0],None)
  #     self.assertIsInstance(t[1],str)
  #     semver.parseVersion.assert_called()
  #     semver.resolveVersion.assert_called()
  #     self.noteManager.retrieve.assert_called()

  # class TestRetrieve(unittest.TestCase):
  #   noteManager =  NoteManager()

  #   @patch('builtins.open',mock_open(read_data='something'))
  #   @patch('occam.notes.manager.json')
  #   def test_should_return_key_given_category_and_key(self,json):
  #     self.noteManager.filePathFor = Mock(return_value = 'FilePathMock')      
  #     json.load.return_value = {"test":"testReturnValue"}
  #     self.noteManager.retrieve("id","category","revision",key="test")
  #     self.noteManager.filePathFor.assert_called()
  #     json.load.assert_called()

  #   @patch('builtins.open',mock_open(read_data='something'))
  #   @patch('occam.notes.manager.json')
  #   def test_should_return_None_when_except_is_caught_and_data_not_dict(self,json):
  #     self.noteManager.filePathFor = Mock(return_value = 'FilePathMock')      
  #     json.load.side_effect = IOError
  #     self.assertEqual(self.noteManager.retrieve("id","category","revision",key="test"),None)
  #     self.noteManager.filePathFor.assert_called()
  #     json.load.assert_called()

  #   @patch('builtins.open',mock_open(read_data='something'))
  #   @patch('occam.notes.manager.json')
  #   def test_should_return_dict_containing_key_and_value_when_json_returns_data_key_is_None_and_data_isinstance_dict(self,json):
  #     self.noteManager.filePathFor = Mock(return_value = 'FilePathMock')      
  #     json.load.return_value = {"something":"another"}
  #     self.assertIsInstance(self.noteManager.retrieve("id","category","revision"),dict)
  #     self.noteManager.filePathFor.assert_called()
  #     json.load.assert_called()

  #   @patch('builtins.open',mock_open(read_data='something'))
  #   @patch('occam.notes.manager.json')
  #   def test_should_return_None_when_data_not_instance_dict_and_fallback_True_and_revision_is_None(self,json):
  #     self.noteManager.filePathFor = Mock(return_value = 'FilePathMock')      
  #     json.load.return_value = ["something"]
  #     self.assertEqual(self.noteManager.retrieve("id","category","revision"),None)
  #     self.noteManager.filePathFor.assert_called()
  #     json.load.assert_called()

  # class TestStore(unittest.TestCase):
  #   noteManager =  NoteManager()

  #   def test_should_return_None_when_data_key_not_a_list(self):
  #     self.noteManager.retrieve = Mock(return_value = {"key":"objectKey","id":"idObject","revision":"revisionObject","name":"nameObject","type":"string"})
  #     self.assertEqual(self.noteManager.store("id","category","key","value"),None)

  #   @patch('occam.notes.manager.json')
  #   def test_should_return_dict_when_not_append_and_token_has_value(self,json):
  #     json.dump.return_value = "ok"
  #     self.noteManager.retrieve = Mock(return_value = {"key":[{"value":"value"}]})
  #     self.noteManager.filePathFor = Mock(return_value = 'filePath')
  #     self.assertIsInstance(self.noteManager.store("id","category","key","value"),dict)

  #   @patch('occam.notes.manager.json')
  #   def test_should_return_dict_when_value_not_in_token_and_remove_index_from_key(self,json):
  #     json.dump.side_effect = IOError
  #     self.noteManager.network.hostname = Mock(return_value = "hostname")
  #     self.noteManager.retrieve = Mock(return_value = {"key":[{"value":"something","vouch":["hostname"]}]})
  #     self.noteManager.filePathFor = Mock(return_value = 'filePath')
  #     self.assertIsInstance(self.noteManager.store("id","category","key","value"),dict)

  #   @patch('occam.notes.manager.json')
  #   def test_should_return_dict_when_retrieve_return_None(self,json):
  #     json.dump.side_effect = IOError
  #     self.noteManager.network.hostname = Mock(return_value = "hostname")
  #     self.noteManager.retrieve = Mock(return_value = None)
  #     self.noteManager.filePathFor = Mock(return_value = 'filePath')
  #     self.assertIsInstance(self.noteManager.store("id","category","key","value"),dict)



