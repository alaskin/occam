# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import importlib
import os
import i18n

from types import SimpleNamespace

from occam.log import Log, loggable

from occam.manager import uses, manager

import sys
import argparse

class Usage(Exception):
  def __init__(self, msg):
    self.msg = msg

from occam.system.manager   import SystemManager
from occam.accounts.manager import AccountManager
from occam.objects.manager  import ObjectManager

@loggable
@uses(SystemManager)
@uses(AccountManager)
@manager("commands")
class CommandManager:
  """
  This handles all of the various Occam commands and registering new commands.
  This also provides the parameter parsing capabilities and is where all
  generic parameters are defined.

  Examples:

    To create a command, write the following in a new file and import it::

      # my_command.py:
      # invoked by: "occam my-component my-command <something_else>+ [-a foo] [-b]"
      from command_manager import command, option, argument

      @command('my-component', 'my-command')
      @argument("something_else", nargs="?")
      @option("-a", "--argument", action = "store",
                                  dest   = "my_argument",
                                  help   = "this is my argument. it is followed by a string.")
      @option("-b", "--boolean",  action = "store_true",
                                  dest   = "my_boolean_argument",
                                  help   = "this turns on something")
      class MyCommand:
        def do(self):
          # Pull out the argument value
          value = self.options.argument

          if self.options.my_boolean_argument:
            # do something
            pass

          for argument in self.options.something_else:
            print(argument)

    And you can run this with: "occam my-component my-command --argument foo --boolean a b c"

    This will print out "a" "b" and "c" since those are normal (positional) arguments.

    Note that all arguments (such as help, dest, etc) to @option and @argument are optional.

    To subclass a command, there are some interesting tricks to consider. When you subclass
    you will inherit all arguments and options of the subclassed command. When you specify
    an option with the same name, you will override the option. It cannot have a different
    destination, short, or long name. You can turn off options by overriding them and setting
    the 'nargs' argument to '0'.

    However, when you do this for a positional argument (one specified by @argument) that is
    required by the subclassed command, you will need to specify a function that will
    satisfy the argument and provide a value. You will use the 'using' argument of @argument
    to do so. A tad involved and confusing, but take a look at this example:

      # my_command.py
      from command_manager import command, option, argument

      @command('my-component', 'my-command')
      @argument("something")
      @option("-a", "--argument", action = "store",
                                  dest   = "my_argument",
                                  help   = "this is my argument. it is followed by a string.")
      @option("-b", "--boolean",  action = "store_true",
                                  dest   = "my_boolean_argument",
                                  help   = "this turns on something")
      class MyCommand:
        def do(self):
          # Pull out the argument value
          value = self.options.argument

          if self.options.my_boolean_argument:
            # do something
            pass

          for argument in self.options.something_else:
            print(argument)

    And subclassing it, as such:

      # my_extra_command.py
      from my_command import MyCommand

      from command_manager import command, option, argument

      @command('my-component', 'my-extra-command')
      @argument("something", nargs="0", using='provideSomething')
      class MyExtraCommand(MyCommand):
        def provideSomething(self):
          return "something!!"

    This `my-extra-command` will do exactly the same things as my-command, except instead of
    expecting the argument to be provided, it is assumed to be "something!!" instead. Since
    we do not specify a `do` function, the command works exactly the same as the base
    command.
  """

  commandList      = {} # Holds the knowledge of possible commands
  options          = {} # Holds the set of optional parameters for each command
  arguments        = {} # Holds the set of positional arguments for each command
  pendingArguments = {} # Holds a set of arguments that have to be computed (via 'using')
  names            = {} # Holds the names of commands by component
  categoryList     = {} # An index for categorical view of commands

  @staticmethod
  def parser(component, command):
    parser = argparse.ArgumentParser("occam %s %s" % (component, command))

    group = parser.add_argument_group('general arguments')

    group.add_argument("-R", "--rootPath", action = "store",
                                           type   = str,
                                           dest   = "rootPath",
                                           help   = "the root path for occam")
    group.add_argument("-V", "--verbose",  action = "store_true",
                                           dest   = "verbose",
                                           help   = "prints all messages")
    group.add_argument("-L", "--logtype", action = "store",
                                          dest   = "logtype",
                                          help   = "the logging text for commands. Possible options are: 'text' (default), and 'json'.")
    #group.add_argument("-O", "--on",      action = "store",
    #                                      dest   = "remote_machine",
    #                                      help   = "runs the command on the given machine written as: username@host:port")
    group.add_argument("-T", "--token",   action = "store",
                                          dest   = "token",
                                          help   = "The authentication token to run the command as the described Person.")
    return parser

  @staticmethod
  def register(component, name, category, documentation, commandHandler):
    """ Registers the command for the given component with the given documentation.

    Typically, you would use the decorator around a
    handler for that command. See documentation for the CommandManager class
    above.
    """

    # Record the command
    CommandManager.commandList[component] = CommandManager.commandList.get(component, {})
    CommandManager.commandList[component][name] = {
      "component":     component,
      "name":          name,
      "handler":       commandHandler,
      "category":      category,
      "documentation": documentation
    }

    # Cross-reference to the command category
    if not category in CommandManager.categoryList:
      CommandManager.categoryList[category] = {}

    CommandManager.categoryList[category][name] = CommandManager.commandList[component][name]

    CommandManager.names[component] = CommandManager.names.get(component, {})
    CommandManager.names[component][commandHandler] = name

  @staticmethod
  def commands(component=None, category=None):
    if component is None:
      #if category is None:
      return list(CommandManager.commandList.keys())

    return list(CommandManager.categoryList.get(category, {}).keys())

  @staticmethod
  def components():
    return list(CommandManager.commandList.keys())

  @staticmethod
  def categories():
    return list(CommandManager.categoryList.keys())

  @staticmethod
  def commandInfo(component, name):
    return CommandManager.commandList[component][name]

  @staticmethod
  def instantiate(component, name, args):
    if component not in CommandManager.commandList:
      return None, None

    if name not in CommandManager.commandList[component]:
      return None, None

    parser  = CommandManager.parser(component, name)
    handler = CommandManager.commandList[component][name]["handler"]

    parser = handler.initParser(parser)
    opts = parser.parse_args(args)

    CommandManager.finalizeOptions(handler, opts)

    return opts, handler

  @staticmethod
  def finalizeOptions(handler, opts):
    # Handle object/revision tag
    for k, v in CommandManager.options[handler].items():
      if 'object' in v and str(v['args'].get('nargs')) != "0":
        arg = getattr(opts, v['args']['dest'])
        newArg = ObjectManager.parseObjectIdentifier(arg)
        setattr(opts, v['args']['dest'], newArg)

    for v in CommandManager.arguments[handler]:
      if 'object' in v and str(v['args'].get('nargs')) != "0":
        arg = getattr(opts, v['name'])
        newArg = ObjectManager.parseObjectIdentifier(arg)
        setattr(opts, v['name'], newArg)

  def execute(self, args, storeOutput=False, stdin=None, stdout=None, stderr=None, allowLocalPerson=True):
    if len(args) == 0:
      CommandManager.Log.error("No component was given")
      return -1

    # The first argument is the component name
    # TODO: Look up the component in the configuration
    try:
      importlib.import_module("occam.%s.manager" % args[0].lower())

      component = args[0]

      if len(args) == 1:
        CommandManager.Log.error("No command was given for component %s" % (args[0]))
        return -1

      command   = args[1]

      args      = args[1:]
    except ImportError as e:
      if "No module" in str(e):
        CommandManager.Log.error("No such component %s" % args[0])
        return -1
      else:
        # Some other issue... bad code in the command script is most likely
        # Make sure it raises
        raise(e)

    try:
      importlib.import_module("occam.%s.commands.%s" % (component.lower(), command.lower()))
    except ImportError as e:
      if "No module" in str(e):
        CommandManager.Log.error("No such command %s in %s" % (command, component))
        return -1
      else:
        # Some other issue... bad code in the command script is most likely
        # Make sure it raises
        raise(e)

    # If the command is valid, invoke it.
    if component in CommandManager.commandList and command in CommandManager.commandList[component]:
      # Form command line parser
      parser = CommandManager.parser(component, command)

      # Add specialized arguments for this command and parse
      opts, handler = CommandManager.instantiate(component, command, args[1:])

      if handler is None:
        CommandManager.Log.error("OCCAM cannot initialize a handler for command %s" % (command))
        return -1

      # Initialize Logger
      Log.initialize(opts, storeOutput=storeOutput, stdout = stdout, stderr = stderr)

      if not self.system.initialized() and not (component == "system" and command == "initialize"):
        Log.error("OCCAM is not initialized.")
        return -1

      person = None
      if self.system.initialized() and not (component == "system" and command == "initialize"):
        if opts.token:
          token = self.accounts.decodeToken(opts.token)

          if 'identity' in token:
            person = self.accounts.currentPerson(token=opts.token)

            if person is None:
              Log.error("Authentication failed.")
              return -1

            Log.noisy("Authenticated as %s" % (person.id))
          elif 'readOnly' in token:
            person = SimpleNamespace(readonly = token['readOnly'])
          elif 'anonymous' in token:
            person = SimpleNamespace(anonymous = token['anonymous'])
        elif allowLocalPerson:
          person = self.accounts.currentPerson()
          if person:
            Log.noisy("Authenticated as %s" % (person.id))

      if stdin is None:
        stdin = sys.stdin.buffer

      try:
        commandImplementation = handler(opts, args, person, stdin)

        # Now perform subclassed command argument parsing
        if handler in CommandManager.pendingArguments:
          for argument in CommandManager.pendingArguments[handler]:
            argument = argument.copy()
            argument['args'] = argument['args'].copy()
            argument['args']['nargs'] = argument.get('original_nargs')
            parser = argparse.ArgumentParser("occam %s %s" % (component, command))
            parser.add_argument(argument['name'], **argument['args'])
            func = argument['using']
            value = getattr(handler, func)(commandImplementation)
            subopts = parser.parse_args([value])
            commandImplementation.options = argparse.Namespace(**vars(commandImplementation.options), **vars(subopts))
            if argument.get('object'):
              arg = getattr(commandImplementation.options, argument['args'].get('dest', argument['name']))
              newArg = ObjectManager.parseObjectIdentifier(arg)
              setattr(commandImplementation.options, argument['args'].get('dest', argument['name']), newArg)

        if os.getenv('OCCAM_PROFILE'):
          outputPath = None

          import cProfile, io

          pr = None

          if stderr:
            # Perform per-thread profile logs when stderr exists
            from occam.config import Config
            import threading

            basePath = Config.root()
            outputPath = os.path.join(basePath, "daemons", f"profile-{os.getpid()}.prof")

            if not hasattr(CommandManager, 'threadLocal'):
              CommandManager.threadLocal = threading.local()

            pr = getattr(CommandManager.threadLocal, 'occam_profiler', None)
            if pr is None:
              pr = cProfile.Profile()
              CommandManager.threadLocal.occam_profiler = pr
          else:
            pr = cProfile.Profile()

          # Run command
          pr.enable()
          code = commandImplementation.do()
          pr.disable()

          # Gather statistics
          import pstats
          from pstats import SortKey

          sortKey = os.getenv('OCCAM_PROFILE').upper()
          if sortKey == "FILE":
            sortKey = -1
            outputPath = f"{component}-{command}.prof"
          elif hasattr(SortKey, sortKey):
            sortKey = getattr(SortKey, sortKey)
          else:
            sortKey = -1

          s = io.StringIO()
          ps = pstats.Stats(pr, stream=s)
          if sortKey != -1:
            ps = ps.sort_stats(sortKey)

          # Output statistics
          if outputPath:
            ps.dump_stats(outputPath)
          else:
            ps.print_stats()
            print(s.getvalue())
        else:
          code = commandImplementation.do()
      except Exception as e:
        if os.getenv('OCCAM_DEBUG'):
          raise e
        Log.error(e)
        return -1

      return code

    # Otherwise, command was not found
    Log.error("Command not found: %s %s" % (component, command))
    return -1

def command(component, name, documentation="", category="Other"):
  """ This decorator will register a command and give the command a way to log.
  """

  def register_command(cls):
    CommandManager.register(component, name, category, documentation, cls)

    if not cls in CommandManager.options:
      CommandManager.options[cls] = {}

    if not cls in CommandManager.arguments:
      CommandManager.arguments[cls] = []

    def initCommand(self, opts, args, person, stdin):
      self.options = opts
      self.args    = args
      self.person  = person
      self.stdin   = stdin

    def initParser(parser):
      arguments = []

      # Now we add base class arguments
      oldnames = {}
      if hasattr(cls.__bases__[0], 'initParser'):
        for v in reversed(CommandManager.arguments[cls.__bases__[0]]):
          if str(v.get('args').get('nargs', '')) == '0':
            continue

          argument = v.copy()
          argument['args'] = v['args'].copy()

          arguments.append(argument)
          oldnames[argument['name']] = argument

      # Add new arguments before these, augmenting the old argument list
      for v in reversed(CommandManager.arguments[cls]):
        # Instead of adding, update metadata
        if v['name'] in oldnames:
          v['original_nargs'] = oldnames[v['name']]['args'].get('nargs')
          if oldnames[v['name']].get('object'):
            v['object'] = True
          oldnames[v['name']]['args'].update(v['args'])
        else:
          if str(v.get('args').get('nargs', '')) == '0':
            continue

          # Add argument to argument parser
          parser.add_argument(v['name'], **v['args'])

      for argument in arguments:
        if str(argument.get('args').get('nargs')) == '0':
          continue

        parser.add_argument(argument['name'], **argument['args'])

      # Again, add base class options
      if hasattr(cls.__bases__[0], 'initParser'):
        names = map(lambda x: x['long'] or x['short'], CommandManager.options[cls].values())
        for k, v in CommandManager.options[cls.__bases__[0]].items():
          if v['short'] not in names and v['long'] not in names:
            if v['short']:
              parser.add_argument(v['short'], v['long'], **v['args'])
            else:
              parser.add_argument(v['long'], **v['args'])
          else:
            for newArg in CommandManager.options[cls].values():
              if v.get('short', v.get('long')) == newArg.get('short') or v.get('short', v.get('long')) == newArg.get('long'):
                v['args'].update(newArg['args'])
                newArg['args'] = v['args']
                newArg['long'] = v.get('long', newArg.get('long'))
                newArg['short'] = v.get('short', newArg.get('short'))

      # Add the (remaining) normal options
      for k, v in CommandManager.options[cls].items():
        if str(v.get('args').get('nargs', '')) == '0':
          continue

        if v.get('short'):
          parser.add_argument(v['short'], v['long'], **v['args'])
        else:
          parser.add_argument(v['long'], **v['args'])

      return parser

    cls.initParser = staticmethod(initParser)
    cls.__init__ = initCommand
    return cls

  return register_command

def argument(name, action="store", help=None, type=None, default="", nargs=None, using=None):
  """ This decorator will add a command line positional argument to the command.
  """

  def addArgument(cls):
    if not cls in CommandManager.arguments:
      CommandManager.arguments[cls] = []

    argInfo = {
      'name':      name,
      'args': {
        'action':  action,
        'default': default,
        'help':    help
      }
    }

    if not using is None:
      argInfo['using'] = using
      if not cls in CommandManager.pendingArguments:
        CommandManager.pendingArguments[cls] = []
      CommandManager.pendingArguments[cls].append(argInfo)

    if not nargs is None:
      argInfo['args']['nargs'] = nargs

    if not type is None:
      if type == "object":
        argInfo['object'] = True
        argInfo['args']['type'] = str
      else:
        argInfo['args']['type'] = type

    CommandManager.arguments[cls].append(argInfo)

    return cls

  return addArgument

def option(name, secondName=None, dest=None, action=None, help=None, type=None, default=None, nargs=None):
  """ This decorator will add a command line option to the command.

  Args:
    name (str): The short name for the option. Must be lowercase. i.e. "-a"
    secondName (str): The long name for the option. i.e. "--all"
    dest (str): The destination variable for the option. i.e. "my_option"
        will be seen in self.options.my_option
    action (str): How to store the values. Can be "store", "store_true", "append".
        See the normal argparse documentation.
    help (str): The help documentation string displayed when "--help" is used.
    type: The type that the argument will be interpreted as. Can be `str` or `int`
        or "object" as a string to interpret the value as an Occam object tag.
    default: The default value.
    nargs: How to interpret the number of possible values that can be given.
        See the normal argparse documentation.
  """

  if secondName is None:
    long  = name
    short = None
  else:
    long = secondName
    short = name

  # Do not allow lowercase short names
  if short and short[1] == short[1].upper():
    raise Exception("Cannot use a capitalized argument in a user command")

  def nargs_range(nmin, nmax, action):
    class RequiredLength(argparse.Action):
      def __call__(self, parser, args, values, option_string=None):
        check_min = nmin
        if nmin is None:
          check_nmin = 0

        check_max = nmax
        if nmax is None:
          check_max = len(values)

        if not check_min <= len(values) <= check_max:
          if not nmin is None and not nmax is None:
            msg = 'option "{f} requires {nmin} and {nmax} arguments'.format(f = self.dest, nmin=nmin, nmax=nmax)
          elif nmin is None:
            msg = 'option "{f} requires at most {nmax} arguments'.format(f = self.dest, nmax=nmax)
          else:
            msg = 'option "{f} requires at least {nmin} arguments'.format(f = self.dest, nmin=nmin)
          raise argparse.ArgumentTypeError(msg)
        if action == "append":
          if not hasattr(args, self.dest):
            setattr(args, self.dest, [])
          lst = getattr(args, self.dest)
          lst.append(values)
          setattr(args, self.dest, lst)
        else:
          action(parser, args, self.dest, values)
    return RequiredLength

  def addOption(cls):
    if not cls in CommandManager.options:
      CommandManager.options[cls] = {}

    actual_nargs  = nargs
    actual_action = action

    if not nargs is None and isinstance(nargs, dict):
      # Action is a ranged nargs
      nmin = nargs.get('min')
      nmax = nargs.get('max')

      if nmin and nmin > 0:
        actual_nargs = "+"
      else:
        actual_nargs = "?"

      actual_action = nargs_range(nmin, nmax, action)

    CommandManager.options[cls][dest] = {
      'short':   short,
      'long':    long,
      'args': {
        'dest':    dest,
        'action':  actual_action,
        'default': default,
        'help':    help
      }
    }

    if not actual_nargs is None:
      CommandManager.options[cls][dest]['args']['nargs'] = actual_nargs

    if not type is None:
      if type == "object":
        CommandManager.options[cls][dest]['object'] = True
        CommandManager.options[cls][dest]['args']['type'] = str
      else:
        CommandManager.options[cls][dest]['args']['type'] = type

    return cls

  return addOption
