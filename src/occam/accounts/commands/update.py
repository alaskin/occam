# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.manager import uses

from occam.accounts.manager import AccountManager
from occam.objects.manager  import ObjectManager

from occam.commands.manager import command, option, argument

@command('accounts', 'update',
  category      = 'Account Management',
  documentation = "Updates an existing account on the local system.")
@option("-u", "--username",    dest    = "username",
                               action  = "store",
                               help    = "look up account by username")
@option("-i", "--identity_uri", dest    = "identity_uri",
                                action  = "store",
                                help    = "look up account by identity URI")
@option("-o", "--add-role",    dest    = "roles",
                               action  = "append",
                               default = [],
                               type    = str,
                               help    = "adds the given role to the account (requires administrator role)")
@option("-r", "--remove-role", dest    = "remove_roles",
                               action  = "append",
                               default = [],
                               type    = str,
                               help    = "removes the given role from the account (requires administrator role)")
@option("-p", "--person", dest = "person",
                          action = "store",
                          type = "object",
                          help = "updates the Person associated with this account.")
@uses(ObjectManager)
@uses(AccountManager)
class UpdateCommand:
  def do(self):
    account = None

    if len(self.options.roles) > 0 or len(self.options.remove_roles) > 0:
      # Only administrators can perform this action
      if 'administrator' not in self.person.roles:
        Log.error("Only an administrator can add roles to an account.")
        return -1

    if self.options.username is None and self.options.identity_uri is None:
      Log.error("Must specify username or identity_uri")
      return -1

    Log.header("Updating account %s" % (self.options.username or self.options.identity_uri))

    account = self.accounts.retrieveAccount(username = self.options.username,
                                            identity = self.options.identity_uri)

    if account is None:
      Log.error("No account found")
      return -1

    new_roles = self.accounts.addRoles(account, self.options.roles)
    new_roles = self.accounts.removeRoles(account, self.options.remove_roles)

    if self.options.person:
      # Retrieve person
      person = self.objects.resolve(self.options.person, person = self.person)
      if person is None:
        Log.error("Cannot retrieve the provided Person")
        return -1
      
      if person.identity != account.identity_key_id:
        Log.error("Person must be owned by the updated account")
        return -1

      Log.write(f"Setting Person to {person.id}")
      self.accounts.updatePerson(account.identity_key_id, person)

    Log.write("Current roles: %s" % (', '.join(new_roles)))

    Log.done("Successfully updated %s" % (account.username))
    return 0
