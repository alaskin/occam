# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Account records are authentication records. They are attached to a Person
# which is a general account of a particular Person/Organization in the
# federation. If a Person has an Account record, they can authenticate to
# the current node. A Person may have an Account on multiple clusters in
# the federation, enabling them to log in to several different nodes.

# When you authenticate to the system, you generate a token. In this case it
# is an HMAC-SHA256 generated using a random 64 byte key. When an Account is
# created (see AccountManager.addAuthentication) that key is generated along
# with an RSA key. (The RSA key is not used for node authentication)

# For terminal access, the login token is implied by the owner of that shell.
# The token is generated upon a "login" command and stored in the shell
# owner's OCCAM home under 'auth.json'. This file is read when any command is
# used and treated as the current Person. (see AccountManager.currentPerson)

# For other access (or to force a particular Person,) a Person UUID and token
# can be passed to any OCCAM command using the "--as" and "--token" (-A, -T)
# arguments. (See CommandManager.execute and CommandManager.parser) In these
# cases, AccountManager.currentPerson is called with those instead of the keys
# found in the auth.json file. This is used for the socket connections and
# authenticated clients. For example, an interactive web server.

# Passwords are used to authenticate, as well. They are used to generate the
# token. Passwords often have less entropy than tokens, so they are point of
# least resistance to break authentication. Stronger authentication might be
# implemented later, such as an external secure authenticator, or multi-factor
# authentication.

# Passwords are stored in the database as a salted hash using bcrypt (can be
# configured to add new algorithms later) and uses a configurable number of
# rounds. Although all passwords are assumed to use the same number of rounds.
# If rounds were to change, all passwords would have to be reset somehow.
# There is no plan on how to do this.

import os


from occam.config  import Config

from occam.manager import uses, manager

from occam.system.manager        import SystemManager
from occam.objects.write_manager import ObjectWriteManager
from occam.databases.manager     import DatabaseManager
from occam.permissions.manager   import PermissionManager
from occam.keys.manager          import KeyManager
from occam.people.write_manager  import PersonWriteManager

@uses(SystemManager)
@uses(ObjectWriteManager)
@uses(DatabaseManager)
@uses(PermissionManager)
@uses(KeyManager)
@uses(PersonWriteManager)
@manager("accounts")
class AccountManager:
  """ Manages Accounts and People within the node or federation.
  """

  def addPerson(self, name, identity, subtypes=[]):
    """ Create a new person for this identity.
    """
    
    from occam.person import Person

    info = None
    if subtypes:
      info = {
        "name": name,
        "type": "person",
        "subtype": subtypes
      }

    person = self.objects.write.create(name, object_type = "person", info=info)

    self.objects.write.store(person, identity)

    # Create the Person record, to attach it to this account
    self.people.write.update(identity, person)

    # Make Person objects public accessible
    personInfo = self.objects.infoFor(person)
    self.permissions.update(id = person.id, canRead=True, canWrite=False, canClone=False)

    # Make Person object writable by that person
    self.permissions.update(id = person.id, identity=person.identity, canRead=True, canWrite=True, canClone=False)

    return person

  def addMember(self, group, member):
    """ Adds an existing Person as a member under another Person

    Args:
        group (Person): The Occam object representing the group to be amended.
        member (Person): The Occam object representing the person to be added.
        person (Person): The Person who is adding the member to the group.

    Returns:
        MembershipRecord: The record reflecting the membership. None if the membership could not be created.
    """
    if self.objects.objectPositionWithin(group, member) is None:
      # Add the item to the object itself
      self.objects.write.addObjectTo(group, subObject=member, create=False)

      # Create the Membership record for access control purposes
      row = self.datastore.insertMembership(group, member)
    else:
      row = self.datastore.retrieveMembershipFor(group, member)

    # Return the MembershipRecord
    return row

  def removeMember(self, group, member):
    """ Removes an existing member, given as a Person, of its base Person.
    """

    self.objects.write.removeObjectFrom(group, member)

    self.datastore.deleteMembership(group, member)

  def retrieveMembershipsFor(self, member):
    """ Retrieves the list of groups for the given member Object.
    """

    return self.datastore.retrieveMembershipObjectsFor(member)

  def retrieveMembershipsOf(self, group):
    """ Retrieves the list of members for the given group.
    """

    return self.datastore.retrieveMembershipObjectsOf(group)

  def retrievePerson(self, identity):
    """ Retrieves the Person object associated with an identity.

    Arguments:
      identity (str): The identity URI to look up.

    Returns:
      Object: The person object associated with this account (or None)
    """

    account_db = self.datastore.retrieveAccount(identity)
    if account_db is None:
      return None

    person = self.objects.retrieve(id = account_db.person_id)
    person.roles = self.rolesFor(account_db)

    return person

  def updatePerson(self, identity, person):
    """ Updates the Person associated with the given Account.
    """

    self.datastore.updateAccount(identity, personId = person.id)
    self.people.write.update(identity, person)

  def retrievePersonObject(self, account_db_object):
    """ Retrieves the augmented (PersonRecord/ObjectRecord) for the given AccountRecord.
    """

    import sql
    session = self.database.session()
    
    objects = sql.Table('objects')
    people  = sql.Table('people')

    join = people.join(objects)
    join.condition = (join.right.id == people.id) & (people.id == account_db_object.person_id)

    query = join.select()

    self.database.execute(session, query)
    record = self.database.fetch(session)

    if record is None:
      return None

    from occam.people.records.person import PersonRecord
    from occam.objects.records.object import ObjectRecord
    return (PersonRecord(record), ObjectRecord(record))

  def retrievePeople(self, id=None, name=None, email=None):
    import sql
    session = self.database.session()
    # import pdb;pdb.set_trace()
    objects = sql.Table('objects')
    people  = sql.Table('people')

    subquery = objects.select(objects.id)
    if name is not None:
      if subquery.where:
        subquery.where = subquery.where | (objects.name == name)
      else:
        subquery.where = (objects.name == name)

    if id is not None:
      if subquery.where:
        subquery.where = subquery.where | (objects.id == id)
      else:
        subquery.where = (objects.id == id)

    query = people.select()
    query.where = (people.id.in_(subquery))

    if email is not None:
      query.where = query.where | (people.email == email)

    self.database.execute(session, query)

    records = self.database.many(session, size=10)

    from occam.people.records.person import PersonRecord

    return [PersonRecord(record) for record in records]

  def retrieveAccount(self, username=None, email=None, person_id=None, identity=None):
    import sql
    session = self.database.session()

    accounts = sql.Table('accounts')

    query = accounts.select()
    query.where = sql.Literal(True)

    if username is not None:
      query.where = query.where & (accounts.username == username)

    if identity is not None:
      query.where = query.where & (accounts.identity_key_id == identity)

    if email is not None:
      query.where = query.where & (accounts.email == email)

    if person_id is not None:
      # Find Account by person_id
      people = sql.Table('people')

      query.where = (accounts.person_id == person_id)

    self.database.execute(session, query)

    record = self.database.fetch(session)

    if record is None:
      return None

    from occam.accounts.records.account import AccountRecord

    return AccountRecord(record)

  def authTokenPath(self):
    auth_token_path = os.path.join(Config.root(), "auth.json")

    return auth_token_path

  def currentAuthentication(self):
    auth_token_path = self.authTokenPath()
    
    if os.path.exists(auth_token_path):
      import json

      try:
        with open(auth_token_path, 'r') as f:
          return json.load(f)
      except:
        pass

    return {}

  def removeAuthentication(self):
    auth_token_path = self.authTokenPath()

    if os.path.exists(auth_token_path):
      os.remove(auth_token_path)

  def generateToken(self, account_db_object, obj = None, access = "readOnly"):
    import jwt

    token = {
      'identity': account_db_object.identity_key_id
    }

    if obj:
      token[access] = obj.id

    secret = account_db_object.secret
    
    encoded = jwt.encode(token, secret, algorithm="HS256").decode('utf-8')
    
    return encoded

  def generateAuthentication(self, account_db_object):
    auth_token_path = self.authTokenPath()

    token = self.generateToken(account_db_object)

    auth_token = {
      'account': account_db_object.username,
      'token':   token
    }

    import json
    
    with open(auth_token_path, 'w+') as f:
      f.write(json.dumps(auth_token))

    return auth_token

  def decodeToken(self, token):
    """ Returns the decoded token information.
    """
    import jwt

    decoded = {}
    try:
      decoded = jwt.decode(token.encode('utf-8'), verify=False, algorithms=['HS256'])
    except:
      decoded = {}

    return decoded

  def currentPerson(self, token=None):
    import jwt

    account_db_object = None
    
    if token is None:
      auth_token = self.currentAuthentication()

      if 'account' in auth_token:
        token      = auth_token.get('token')

    if token is None:
      return None

    try:
      unverified = jwt.decode(token.encode('utf-8'), verify=False, algorithms=['HS256'])
    except:
      unverified = {}
    account_db_object = self.retrieveAccount(identity = unverified.get('identity'))

    if account_db_object is None:
      return None
    
    # Authenticate the token
    try:
      payload = jwt.decode(token.encode('utf-8'), account_db_object.secret, algorithms=['HS256'])
    except:
      payload = {}

    return self.retrievePerson(payload.get('identity'))

  def currentPersonId(self):
    person = self.currentPerson()

    if person:
      return person.id

    return None

  def authenticate(self, username=None, email=None, password=None):
    account = self.retrieveAccount(username=username, email=email)
    
    if account is None:
      return False

    import bcrypt  # For password hashing
    try:
      return bcrypt.hashpw(password.encode('utf-8'), account.hashed_password.encode('utf-8')) == account.hashed_password.encode('utf-8')
    except AttributeError:
      raise AttributeError("Password must not be None, int, or float.")

  def rolesFor(self, account_db):
    """ Returns the roles for the given account.
    """
    
    # Gather existing roles (they are ; delimited)
    try:
      roles = (account_db.roles or ";;")[1:-1].split(';')
    except TypeError:
      raise TypeError

    # Ensure an empty list is properly handled
    if len(roles) == 1 and roles[0] == "":
      roles = []

    return roles

  def addRoles(self, account_db, newRoles=[]):
    """ Adds the given roles to the given AccountRecord
    """

    roles = self.rolesFor(account_db)

    # Append given roles
    roles.extend(newRoles)

    return self.commitRoles(account_db, roles)

  def removeRoles(self, account_db, newRoles=[]):
    """ Removes roles that are not in newRoles
    """
    from occam.accounts.records.account import AccountRecord
    
    if not isinstance(account_db,AccountRecord):
      raise Exception("The argument should be an AccountRecord instance")

    roles = self.rolesFor(account_db)

    # Get the difference in the role set
    roles = list(set(roles) - set(newRoles))

    return self.commitRoles(account_db, roles)

  def commitRoles(self, account_db, roles=[]):
    # Remove duplicates
    roles = list(set(roles))

    # Remove empty roles
    roles = list(filter(None, roles))

    # String-ify
    try:
      account_db.roles = ";%s;" % (';'.join(roles))
    except TypeError:
      raise TypeError
    
    # Commit changes to database
    session = self.database.session()
    self.database.update(session, account_db)
    self.database.commit(session)

    return roles

  def addAuthentication(self, person_id, username, password, identityKey, roles=[]):
    from occam.accounts.records.account import AccountRecord

    # Get system admin record
    system = self.system.retrieve()

    # Set password (if exists)
    account = AccountRecord()

    # Accounts are active by default, unless moderation says otherwise
    if system.moderate_accounts:
      # We are moderating accounts
      account.active = False
    else:
      # We allow all accounts
      account.active = True

    # Default to no roles
    account.roles = ";;"

    # First account defaults to administrator role
    import sql
    accounts = sql.Table('accounts')

    session = self.database.session()

    query = accounts.select(sql.aggregate.Count(sql.Literal(1)))
    self.database.execute(session, query)
    accountCount = next(iter(self.database.fetch(session).values()))

    if accountCount == 0:
      if not 'administrator' in roles:
        roles.append('administrator')

    # Add the roles designated by the admin table
    default_roles = ((system.default_roles or ";;")[1:-1] or "").split(';')
    for default_role in default_roles:
      if not default_role in roles:
        roles.append(default_role)

    roles = list(set(roles))

    # Add the semi-colon delimited list of roles
    # TODO: roles can be broken if they contain a ';'
    #       but maybe this is not a problem.
    account.roles = ';' + ';'.join(roles) + ';'

    # Add username
    account.username = username

    account.person_id = person_id

    # Create password auth
    account.hashed_password = self.hashPassword(password)

    account.identity_key_id = identityKey.uri

    account.secret = self.keys.randomHex(64)

    self.database.update(session, account)
    self.database.commit(session)

  def hashPassword(self, password):
    """ Generates a password hash for the given string.
    """
    rounds = 12    
    config_info = self.configuration
    rounds = config_info.get('passwords', {}).get('rounds', rounds)

    import bcrypt   # For password hashing
    try:
      return bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt(rounds, prefix=b"2a")).decode('utf-8')
    except AttributeError:
      raise AttributeError("Password should not be None, int, float.")

  def updatePassword(self, account, password):
    """ Updates the password for the given account.
    """

    session = self.database.session()

    # Create password auth
    account.hashed_password = self.hashPassword(password)

    self.database.update(session, account)
    self.database.commit(session)

  def authorsFor(self, uuid):
    """ Returns a PersonRecord list for all People that are authors of the given object.
    """

    return self.datastore.retrieveAuthors(uuid)

  def collaboratorsFor(self, uuid):
    """ Returns a PersonRecord list for all People that are collaborators of the given object.
    """

    return self.datastore.retrieveAuthors(uuid, kind="collaboratorships")

  def removeAuthorship(self, id, identity):
    """ Removes the given authorship.

    Arguments:
      id (str): The object id.
      identity (str): The identity of the author to remove from the object.
    """

    self.datastore.removeAuthor(id, identity)

  def removeCollaboratorship(self, id, identity):
    """ Removes the given collaboratorship.

    Arguments:
      id (str): The object id.
      identity (str): The identity of the collaborator to remove from the object.
    """

    self.datastore.removeAuthor(id, identity, kind = "collaboratorships")

  def addAuthorship(self, id, identity):
    """ Adds the given identity as an author of the given Object.

    Arguments:
      id (str): The object id.
      identity (str): The identity of the author to add to the object.
    """

    self.datastore.addAuthor(id, identity)

  def addCollaboratorship(self, id, identity):
    """ Adds the given identity as a collaborator of the given Object.

    Arguments:
      id (str): The object id.
      identity (str): The identity of the collaborator to add to the object.
    """

    self.datastore.addAuthor(id, identity, kind = "collaboratorships")
