# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql

from occam.databases.manager import uses, datastore

from occam.objects.database import ObjectDatabase

@uses(ObjectDatabase)
@datastore("accounts")
class AccountDatabase:
  """ Manages the database interactions for the Account component.
  """

  def queryMembershipsFor(self, identity, key = None, keyAs = None):
    """ Retrieves a query the returns a row for each MembershipRecord associated with the given Person Object.

    Arguments:
      identity (str): The identity to query memberships for.
      key (str): The key (if any) to retrieve for the query.
      keyAs (str): What to rename the key as.
    """

    if keyAs is None:
      keyAs = key

    memberships = sql.Table("memberships")

    if key is None:
      query = memberships.select()
    else:
      query = memberships.select(memberships.__getattr__(key).as_(keyAs))

    query.where = (memberships.identity_uri == identity)
    query.where = query.where & (memberships.reference_count > 0)

    return query

  def queryMembershipFor(self, group, member):
    """ Retrieves a query for the specific row pertaining to the MembershipRecord
        associated with the given Person group and Person member.

    Arguments:
      group (str): The identity associated with the group.
      member (str): The identity to look up within the group.

    Returns:
      sql.Query: The query to retrieve the membership record.
    """

    query = self.queryMembershipsFor(member)
    query.where = query.where & (memberships.base_identity_uri == group)

    return query

  def retrieveMembershipsFor(self, identity):
    """ Retrieves a list of each MembershipRecord associated with the given identity.

    Arguments:
      identity (str): The identity URI to look up.

    Returns:
      list: A set of MembershipRecord items for each membership of this identity.
    """

    from occam.accounts.records.membership import MembershipRecord

    session = self.database.session()

    query = self.queryMembershipsFor(identity)

    self.database.execute(session, query)

    records = self.database.many(session, size=1000)
    return [MembershipRecord(x) for x in records]

  def retrieveMembershipObjectsFor(self, person):

    from occam.accounts.records.membership import MembershipRecord

    session = self.database.session()

    query = self.queryMembershipsFor(person, key = "base_identity_uri", keyAs = "id")
    return self.objects.retrieveObjectsForIds(query)

  def queryMembershipsOf(self, identity, key = None, keyAs = None):
    """ Retrieves a query the returns a row for each MembershipRecord associated
        with the given (group) identity.

    Arguments:
      identity (str): The identity URI to look up for the group.
      key (str): The key, if any, to retrieve from the record.
      keyAs (str): What to rename that key to.

    Returns:
      sql.Query: The query to retrieve all membership records for given identity.
    """

    if keyAs is None:
      keyAs = key

    memberships = sql.Table("memberships")

    if key is None:
      query = memberships.select()
    else:
      query = memberships.select(memberships.__getattr__(key).as_(keyAs))

    query.where = (memberships.base_identity_uri == identity)
    query.where = query.where & (memberships.reference_count > 0)

    return query

  def retrieveMembershipsOf(self, identity):
    """ Retrieves a list of each MembershipRecord that has the given identity as a member.

    Arguments:
      identity (str): The identity to look up.
    """

    from occam.accounts.records.membership import MembershipRecord

    session = self.database.session()

    query = self.queryMembershipsOf(identity)

    self.database.execute(session, query)

    records = self.database.many(session, size=1000)
    return [MembershipRecord(x) for x in records]

  def _retrieveMembershipObjectsOf(self, identity):
    """ Returns the Object representing the members of the given group.
    """

    from occam.accounts.records.membership import MembershipRecord

    session = self.database.session()

    query = self.queryMembershipsOf(person, key = "identity_uri", keyAs = "id")
    return self.objects.retrieveObjectsForIds(query)

  def retrieveMembershipFor(self, group, member):
    """ Returns the MembershipRecord for the given group and member.

    Arguments:
      group (str): The identity URI for the group.
      member (str): The identity URI for the member of the group.

    Returns:
      MembershipRecord: The record of the membership of the member with the group.
    """

    from occam.accounts.records.membership import MembershipRecord

    session = self.database.session()

    query = self.queryMembershipFor(group, member)

    self.database.execute(session, query)
    record = self.database.fetch(session)
    if record:
      record = MembershipRecord(record)

    return record

  def insertMembership(self, group, member):
    import sql.conditionals

    # Create Membership record to link group to the member

    # Then, create a Membership record to link that member to all owners of that group
    # Or, increment the record if it already exists (since you can be a member
    #   of a particular subgroup more than once through different intermediaries)
    # Poll until the record is created/updated.
    
    # This SQL query will perform an upsert with an increment in SQLite3
    #"with new (identity_uri, base_identity_uri) as ( values(%, %) ) insert or replace into memberships (id, person_object_id, base_identity_uri, reference_count) select old.id, new.person_object_id,  new.base_identity_uri, (old.reference_count+1) from new left join memberships as old on new.person_object_id = old.person_object_id and new.base_identity_uri = old.base_identity_uri"

    # Or??
    #"begin; insert into blah if not exists(); update blah = blah + 1; commit;"

    # Or this:
    #"insert or replace into memberships (id, identity_uri, base_identity_uri, reference_count) values ((coalesce((select id from memberships where person_object_id = 0 and base_identity_uri = 1), NULL)), 0, 1, coalesce((select reference_count from memberships where person_object_id = 0 and base_identity_uri = 1), 0) + 1);"

    # We go for, instead however, a two command transaction, for ease of implementation and to conform with python-sql
    # and the most portable SQL:

    session  = self.database.session()

    memberships = sql.Table("memberships")

    objectQuery = self.objects.queryObjectRecordFor(member, key = "id")
    baseObjectQuery = self.objects.queryObjectRecordFor(group, key = "id")
    baseMembershipQuery = self.queryMembershipsFor(group).select(memberships.base_identity_uri)

    # Insert the initial record for the given member and group

    query = memberships.insert()
    query.columns = [memberships.identity_uri, memberships.base_identity_uri]
    query.values = sql.Select(columns = [objectQuery, baseObjectQuery])
    query.values.where = sql.operators.Not(sql.operators.Exists(memberships.select(sql.Literal(1), where = (memberships.identity_uri.in_(objectQuery)) & (memberships.base_identity_uri.in_(baseObjectQuery)))))

    # INSERT INTO memberships (identity_uri, base_identity_uri) VALUES
    #   (SELECT <objectQuery.id>, <baseObjectQuery.id> WHERE NOT EXISTS
    #     (SELECT 1 FROM 'memberships' WHERE 
    #        memberships.identity_uri IN <objectQuery> AND memberships.base_identity_uri IN <baseObjectQuery>))
    # (IN is used in the final clause because the sql query creation is messed up when it is '=')
    self.database.execute(session, query)

    # Insert the extra records for membership in the group's groups

    # We need to reference the table a different way so it gets a new alias (that is, it is assigned a different "AS x")
    membershipsB = sql.Table("memberships")
    query = memberships.insert()
    query.columns = [memberships.identity_uri, memberships.base_identity_uri]
    subQuery = memberships.select(objectQuery, memberships.base_identity_uri)
    subQuery.where = (memberships.identity_uri.in_(baseObjectQuery))
    subQuery.where = subQuery.where & sql.operators.Not(sql.operators.Exists(membershipsB.select(sql.Literal(1), where = (membershipsB.identity_uri.in_(objectQuery)) & (membershipsB.base_identity_uri == (memberships.base_identity_uri)))))
    query.values = subQuery

    # INSERT INTO memberships (identity_uri, base_identity_uri) VALUES
    #   (SELECT <objectQuery.id>, base_identity_uri AS 'a' WHERE
    #     a.identity_uri = <baseObjectQuery.id> AND NOT EXISTS
    #       (SELECT 1 FROM 'memberships' AS 'b' WHERE b.identity_uri IN <objectQuery> AND
    #         b.base_identity_uri = a.base_identity_uri))
    self.database.execute(session, query)

    # Increment the records

    query = memberships.update(columns = [memberships.reference_count], values=[memberships.reference_count + sql.Literal(1)])

    query.where = (memberships.identity_uri.in_(objectQuery))
    query.where = query.where & ((memberships.base_identity_uri.in_(baseMembershipQuery)) | (memberships.base_identity_uri.in_(baseObjectQuery)))

    # UPDATE 'memberships' (reference_count) VALUES (reference_count + 1) WHERE
    #   identity_uri = <objectQuery.id> AND
    #   (base_identity_uri IN (<baseMembershipQuery>) OR
    #     base_identity_uri = <baseObjectQuery.id>) AND
    #   reference_count > 0
    self.database.execute(session, query)

    # Retrieve the field
    query = self.queryMembershipFor(group, member)
    self.database.execute(session, query)

    # Perform the transaction
    self.database.commit(session)

    record = self.database.fetch(session)
    if record:
      from occam.accounts.records.membership import MembershipRecord
      record = MembershipRecord(record)

    return record

  def deleteMembership(self, group, member):
    """ Removes the member from the given group.
    """

    # Gather all membership records pointing from that person to the group in question
    # Decrement their reference counts
    # Delete them if they are now zero

    session  = self.database.session()

    memberships = sql.Table("memberships")

    # Decrement reference counts for the existing records
    # First, the one between member and group
    # And also, all memberships between member and base objects of group
    # This is easier if there exists a membership between person -> themselves

    query = memberships.update(columns = [memberships.reference_count], values = [memberships.reference_count - sql.Literal(1)])

    objectQuery = self.objects.queryObjectRecordFor(member, key = "id")
    baseObjectQuery = self.objects.queryObjectRecordFor(group, key = "id")
    baseMembershipQuery = self.queryMembershipsFor(group).select(memberships.base_identity_uri)

    query.where = (memberships.identity_uri.in_(objectQuery))
    query.where = query.where & ((memberships.base_identity_uri.in_(baseMembershipQuery)) | (memberships.base_identity_uri.in_(baseObjectQuery)))
    query.where = query.where & (memberships.reference_count > 0)

    # UPDATE 'memberships' (reference_count) VALUES (reference_count - 1) WHERE
    #   identity_uri = <objectQuery.id> AND
    #   (base_identity_uri IN (<baseMembershipQuery>) OR
    #     base_identity_uri = <baseObjectQuery.id>) AND
    #   reference_count > 0
    self.database.execute(session, query)

    # Delete all memberships with 0 in the reference_count

    # If another query beats us to this and upserts the reference_count + 1, this is ok
    # This will only trigger when the reference_count is in fact 0
    # We don't necessarily care that this record is destroyed as we only pull out
    # records when the reference_count > 0, but clean-up is good and can obscure
    # membership when people are removed, which may be useful for privacy etc.
    # (Although, the transaction should be atomic, so not a real worry)
    query = memberships.delete()

    query.where = (memberships.identity_uri.in_(objectQuery))
    query.where = query.where & (memberships.reference_count == 0)

    # DELETE FROM 'memberships' WHERE
    #   identity_uri IN <objectQuery> AND
    #   reference_count = 0
    self.database.execute(session, query)

    # Commit the transaction
    self.database.commit(session)

  def queryAccounts(self, key = None):
    """ Returns a general SQL query for all account records.

    Arguments:
      key (str): The key to pull from the query.

    Returns:
      sql.Query: The SQL query to retrieve any AccountRecord.
    """

    accounts = sql.Table("accounts")
    if key:
      query = accounts.select(accounts.__getattr__(key))
    else:
      query = accounts.select()

    return query

  def queryAccount(self, identity, key = None):
    """ Returns an SQL query to retrieve the Account for the given identity.

    Arguments:
      identity (str or sql.Query): The URI of the identity or a query.
      key (str): The key to pull from the query.

    Returns:
      sql.Query: The SQL query to retrieve the AccountRecord.
    """

    accounts = sql.Table("accounts")
    if key:
      query = accounts.select(accounts.__getattr__(key))
    else:
      query = accounts.select()

    if not isinstance(identity, str) and identity is not None:
      query.where = (accounts.identity_key_id.in_(identity))
    else:
      query.where = (accounts.identity_key_id == identity)

    return query

  def queryPeople(self):
    """ Returns an SQL query to retrieve all Objects representing the Person
        for all accounts.
    """

    subQuery = self.queryAccounts(key = "person_id")

    return self.objects.queryObjects(id = subQuery)

  def queryPerson(self, identity):
    """ Returns an SQL query to retrieve the Object for the Person associated
        with the given identity.

    Arguments:
      identity (str): The URI of the identity.

    Returns:
      sql.Query: The SQL query to retrieve the ObjectRecord.
    """

    subQuery = self.queryAccount(identity, key = "identity_uri")

    return self.objects.queryObjects(id = subQuery)

  def retrieveAccount(self, identity):
    """ Retreives the Account for the given identity.

    Arguments:
      identity (str): The identity URI of the account.

    Returns:
      AccountRecord: The information about the account.
    """

    from occam.accounts.records.account import AccountRecord

    session = self.database.session()

    query = self.queryAccount(identity)

    self.database.execute(session, query)
    row = self.database.fetch(session)
    if row is None:
      return None

    return AccountRecord(row)

  def updateAccount(self, identity, roles=None, personId=None):
    """ Updates the Account record with the given information.

    Arguments:
      identity (str): The identity URI of the account.
      roles (list): A list of strings containing the roles for the account.
      personId (str): The unique ID of the Person to associate to this Account.
    """

    from occam.accounts.records.account import AccountRecord

    session = self.database.session()

    query = self.queryAccount(identity)

    # Retrieve the record
    self.database.execute(session, query)

    row = self.database.fetch(session)
    if row is None:
      return

    # Set fields within row
    row = AccountRecord(row)

    if personId:
      row.person_id = personId

    if roles:
      currentRoles = row.roles[1:-1]
      if currentRoles == '':
        currentRoles = []
      else:
        currentRoles = currentRoles.split(';')

      currentRoles.extend(roles)
      row.roles = ';' + ';'.join(currentRoles) + ';'

    # Update
    self.database.update(session, row)

    # Commit the transaction
    self.database.commit(session)

  def queryAuthors(self, id, identity = None, kind = "authorships", delete=False):
    """ Retrieves the sql.Query to get every author of the given object.

    Arguments:
      id (str): The object id.
      kind (str): The type of author relationship.

    Returns:
      sql.Query: The query to retrieve every Object for the author list.
    """

    authorships = sql.Table(kind)

    query = authorships.select(authorships.identity_uri)
    if delete:
      query = authorships.delete()

    query.where = authorships.internal_object_id == id

    if identity:
      query.where = query.where & (authorships.identity_uri == identity)

    return self.objects.queryObjects(id = query)

  def retrieveAuthors(self, id, kind = "authorships"):
    """ Retrieves the person Objects for every author of the given object.

    Arguments:
      id (str): The object id.
      kind (str): The type of author relationship.

    Returns:
      list: A list of ObjectRecord items for each author.
    """

    from occam.objects.records.object import ObjectRecord

    session = self.database.session()

    query = self.queryAuthors(id, kind = kind)

    self.database.execute(session, query)

    rows = self.database.many(session, size=20)
    return list(map(lambda x: ObjectRecord(x), rows))

  def addAuthor(self, id, identity, kind = "authorships"):
    """ Creates a record associating the given object with the given identity.

    Arguments:
      id (str): The object id.
      identity (str): The identity URI to add as an author.
    """

    from occam.accounts.records.authorship import AuthorshipRecord
    from occam.accounts.records.collaboratorship import CollaboratorshipRecord

    session = self.database.session()

    if kind == "authorships":
      record = AuthorshipRecord()
    else:
      record = CollaboratorshipRecord()

    record.identity_uri = identity
    record.internal_object_id = id

    self.database.update(session, record)
    self.database.commit(session)

  def removeAuthor(self, id, identity, kind = "authorships"):
    """ Removes the record associating the given object with the given identity.

    Arguments:
      id (str): The object id.
      identity (str): The identity URI to remove as an author.
    """

    session = self.database.session()

    query = self.queryAuthors(id, identity = identity, kind = kind, delete = True)
    self.database.execute(session, query)
