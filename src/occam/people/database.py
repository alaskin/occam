# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql

from occam.databases.manager import uses, datastore

@datastore("people")
class PersonDatabase:
  """ Manages the database interactions for the Key component.
  """

  def queryPerson(self, uri = None, key = None):
    """ Returns a query that returns the PersonRecord for the given identity.

    Args:
      uri (str) The identity URI. (Default: None, select all records)
      key (str) A key to select for the query. (Default: None, select all keys)
    """

    people = sql.Table("people")

    if key:
      query = people.select(people.__getattr__(key))
    else:
      query = people.select()

    if uri:
      query.where = (people.identity_uri == uri)

    return query

  def retrievePerson(self, uri):
    """ Returns the PersonRecord for the given identity.

    Args:
      uri (str) The identity URI.
    """

    from occam.people.records.person import PersonRecord

    session = self.database.session()

    query = self.queryPerson(uri)

    self.database.execute(session, query)
    
    row = self.database.fetch(session)
    if row:
      row = PersonRecord(row)

    return row

  def retrievePersonAll(self, uri=None):
    """ Returns all PersonRecords optionally associated with the given identity.

    Args:
      uri (str) The identity URI.
    """

    from occam.people.records.person import PersonRecord

    session = self.database.session()

    query = self.queryPerson(uri = uri)

    self.database.execute(session, query)
    
    rows = self.database.many(session, size=100)
    if rows:
      rows = [PersonRecord(row) for row in rows]

    return rows
