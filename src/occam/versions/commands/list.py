# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log    import Log
from occam.object import Object

from occam.commands.manager import command, option, argument

from occam.objects.manager  import ObjectManager
from occam.versions.manager import VersionManager

from occam.manager import uses

@command('versions', 'list',
  category      = 'Version Management',
  documentation = "Lists the known versions of the given object.")
@argument("object", type="object", nargs="?")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@uses(ObjectManager)
@uses(VersionManager)
class VersionsListCommand:
  def do(self):
    Log.header("Listing tags")

    if self.options.object is None:
      obj = Object(path=".")
    else:
      obj = self.objects.resolve(self.options.object,
                                 person = self.person)

    if obj is None:
      Log.error("Cannot find object")
      return -1

    # List existing tags
    tags = self.versions.retrieve(obj)

    if not tags and not self.options.to_json:
      Log.write("No tags")

    if self.options.to_json:
      import json
      import base64

      json_tags={"versions":[]}

      for tag in tags:
        json_tags["versions"].append({
          "version": tag.tag,
          "revision": tag.revision,
          "identity": tag.identity_uri,
          "published": tag.published.isoformat() + "Z",
          "signature": {
            "data": base64.b64encode(tag.signature).decode('utf-8'),
            "encoding": "base64",
            "format": tag.signature_type or "PKCS1_v1_5",
            "digest": tag.signature_digest or "SHA512",
            "key": tag.verify_key_id
          }
        })
      Log.output(json.dumps(json_tags))
    else:
      for tag in tags:
        Log.write("%s: %s" % (tag.tag, tag.revision))

    return 0
