# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql

from occam.databases.manager import uses, datastore
from occam.databases.manager import DataNotUniqueError

from occam.associations.database import AssociationDatabase

@datastore("associations.write", reader=AssociationDatabase)
class AssociationWriteDatabase:
  """ Manages the database interactions for creating/removing associations.
  """

  def createAssociation(self, object, associationType, major, minor, identity, published):
    """ Creates a new association based on the given criteria.
    """

    from occam.associations.records.association import AssociationRecord

    # Create database session
    session  = self.database.session()

    record = AssociationRecord()

    record.internal_object_id = object.id
    record.revision = object.revision
    record.major = major
    record.minor = minor
    record.association_type = associationType
    record.published = published

    if identity:
      record.identity_uri = identity

    try:
      self.database.update(session, record)
      self.database.commit(session)
    except DataNotUniqueError as e:
      pass

    return record

  def removeAssociation(self, object, associationType, major, minor, identity):
    """ Destroys the association with the given criteria, if it exists.
    """

    # Create database session
    session  = self.database.session()

    associations = sql.Table('associations')
    query = associations.delete()

    query.where = associations.association_type == associationType
    query.where = query.where & (associations.major == major)
    query.where = query.where & (associations.internal_object_id == object.id)

    if minor:
      query.where = query.where & (associations.minor == minor)
    else:
      query.where = query.where & (associations.minor == sql.Null)

    if identity:
      query.where = query.where & (associations.identity_uri == identity)
    else:
      query.where = query.where & (associations.identity_uri == sql.Null)

    self.database.execute(session, query)
    self.database.commit(session)

    return query
