# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql

from occam.databases.manager import uses, datastore

from occam.objects.database import ObjectDatabase

@uses(ObjectDatabase)
@datastore("associations")
class AssociationDatabase:
  """ Manages the database interactions for association management.
  """

  def queryAssociations(self, object          = None,
                              associationType = None,
                              major           = None,
                              minor           = None,
                              identity        = None,
                              systemWide      = False):
    """ Returns an SQL query to find AssociationRecords based on the given criteria.

    When any argument is given, it will be used to filter the resulting query.

    If any argument to this function is None, then that key will not be filtered.
    Therefore, to filter results to rows that do not have a identity URI, you would
    set the systemWide argument to True while setting identity to None.

    If both identity is given and systemWide is True, it will yield rows that either
    are set to that identity or are Null.
    """

    associations = sql.Table('associations')
    query = associations.select()
    query.where = sql.Literal(True)

    if associationType:
      query.where = query.where & (associations.association_type == associationType)

    if major:
      query.where = query.where & (associations.major == major)

    if minor:
      query.where = query.where & (associations.minor == minor)

    if systemWide:
      if identity:
        query.where = query.where & ((associations.identity_uri == identity) | (associations.identity_uri == sql.Null))
      else:
        query.where = query.where & (associations.identity_uri == sql.Null)
    elif identity:
      query.where = query.where & (associations.identity_uri == identity)

    if object:
      query.where = query.where & (associations.internal_object_id == object.id)

    # Order youngest to oldest
    query.order_by = sql.Desc(associations.published)

    return query

  def retrieveAssociations(self, object          = None,
                                 associationType = None,
                                 major           = None,
                                 minor           = None,
                                 identity        = None,
                                 systemWide      = False,
                                 full            = False):
    """ Retrieves a list of all associations for the given identity.

    This can be filtered by the other arguments. The object argument will filter
    associations to those that match that object.

    When full is specified, the row also contains the known normalized object
    metadata associated with the object attached to the association as marked
    in the row.
    """

    from occam.associations.records.association import AssociationRecord

    # Create database session
    session  = self.database.session()

    # Retrieve query
    query = self.queryAssociations(object          = object,
                                   associationType = associationType,
                                   major           = major,
                                   minor           = minor,
                                   identity        = identity,
                                   systemWide      = systemWide)

    if full:
      # Use the query as a subquery and join with the objects table
      objects = sql.Table('objects')
      join = query.join(objects)
      join.condition = (query.internal_object_id == join.right.id)
      query = join.select()

    # Execute query
    self.database.execute(session, query)

    # Retrieve rows
    rows = self.database.many(session, size=100)
    if rows:
      if full:
        from occam.objects.records.object import ObjectRecord
        def mergeRows(row):
          ret = AssociationRecord(row)
          ret.object = ObjectRecord(row)
          return ret

        rows = [mergeRows(row) for row in rows]
      else:
        rows = [AssociationRecord(row) for row in rows]

    return rows

  def retrieveAssociation(self, associationType, major, minor, identity):
    """ Retrieves the first known association to match the given criteria.
    """

    from occam.associations.records.association import AssociationRecord

    # Create database session
    session  = self.database.session()

    # Retrieve query
    query = self.queryAssociations(associationType = associationType,
                                   major           = major,
                                   minor           = minor,
                                   identity        = identity,
                                   systemWide      = identity is None)

    # Execute query
    self.database.execute(session, query)

    # Retrieve row
    row = self.database.fetch(session)

    ret = None
    if row:
      ret = AssociationRecord(row)

    return ret
