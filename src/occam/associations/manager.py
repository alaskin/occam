# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.log     import loggable
from occam.manager import manager, uses

@loggable
@manager("associations")
class AssociationManager:
  """ This OCCAM manager keeps track of associations between objects.
  """

  def retrieveAll(self, associationType, major, minor, identity=None, systemWide=False, full=False):
    """ Retrieves all matching associations.
    """

    return self.datastore.retrieveAssociations(associationType = associationType,
                                               major           = major,
                                               minor           = minor,
                                               identity        = identity,
                                               systemWide      = systemWide,
                                               full            = full)

  def retrieve(self, associationType, major, minor, identity=None):
    """ Retrieves the best known association.
    """

    return self.datastore.retrieveAssociation(associationType = associationType,
                                              major           = major,
                                              minor           = minor,
                                              identity        = identity,
                                              systemWide      = True)
