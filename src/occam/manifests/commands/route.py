# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log              import Log
from occam.object           import Object
from occam.manager          import uses

from occam.objects.manager   import ObjectManager
from occam.manifests.manager import ManifestManager
from occam.backends.manager  import BackendManager

from occam.commands.manager   import command, option, argument

import json

@command('manifests', 'route',
  category      = 'Running Objects',
  documentation = "Determines a path from a given object and backend or goal.")
@argument("object", type = "object", nargs = '?')
@option("-e", "--environment",
  action  = "store",
  dest    = "environment",
  help    = "the environment of the object")
@option("-a", "--architecture",
  action  = "store",
  dest    = "architecture",
  help    = "the architecture of the object")
@option("--target-environment",
  action  = "append",
  default = [],
  dest    = "target_environments",
  help    = "the environment the VM would run on")
@option("--target-architecture",
  action  = "append",
  default = [],
  dest    = "target_architectures",
  help    = "the architecture the VM would run on")
@option("-b", "--target-backend",
  action  = "append",
  default = [],
  dest    = "target_backends",
  help    = "the backend the VM would run on")
@option("-z", "--truth",
  action  = "store_true",
  dest    = "truth",
  help    = "only reports whether or not a path exists for each target pair")
@option("--phase",
  action  = "store",
  default = "run",
  dest    = "phase",
  help    = "the phase to target (run, build)")
@option("-r", "--running",
  action  = "store",
  type    = "object",
  dest    = "running_object",
  help    = "the object to run using the running object")
@uses(ObjectManager)
@uses(ManifestManager)
@uses(BackendManager)
class ManifestPathCommand:
  """ This command generates a list of objects that route an object to a backend or target.
  """

  def do(self, recursive=False):
    environment  = self.options.environment
    architecture = self.options.architecture

    objectId       = self.options.object.id
    objectRevision = self.options.object.revision

    objectInfo = None

    targetEnvironment = None
    if len(self.options.target_environments) > 0:
      targetEnvironment  = self.options.target_environments[0]

    targetArchitecture = None
    if len(self.options.target_architectures) > 0:
      targetArchitecture = self.options.target_architectures[0]

    if environment is None or architecture is None:
      # Pull the environment and architecture from the object
      if objectId is None:
        Log.error("Either an object uuid or an explicit environment/architecture is required.")
        return -1

      object = self.objects.resolve(self.options.object, person=self.person)
      if object is None:
        # Cannot find the object
        Log.error("Cannot find the given object.")
        return -1

      try:
        objectInfo = self.objects.infoFor(object)
        environment = objectInfo.get('environment')
        environment = objectInfo.get(self.options.phase, {}).get('environment', environment)
        architecture = objectInfo.get('architecture')
        architecture = objectInfo.get(self.options.phase, {}).get('architecture', architecture)
        objectRevision = object.revision
      except IOError:
        Log.error("Cannot find the given revision of this object.")
        return -1

      if environment is None or architecture is None:
        Log.error("Specified object does not have an environment or architecture listed.")
        return -1

    if objectInfo is None:
      objectInfo = {
        "environment":  environment,
        "architecture": architecture
      }


    path = []
    if environment != targetEnvironment and architecture != targetArchitecture:
      # Generate task path
      path = self.backends.providerPath(environment, architecture,
                                        environmentGoal  = targetEnvironment,
                                        architectureGoal = targetArchitecture,
                                        person           = self.person)

    if path is None:
      Log.output(json.dumps([]))
      return 0;

    if self.options.truth:
      Log.output(json.dumps(len(path) > 0))
      return 0;

    def formatObject(obj):
      ret = {}
      if hasattr(obj, "revision"):
        ret["id"] = obj.id
        ret["revision"] = obj.revision
        info = self.objects.infoFor(obj)
        ret["name"] = info["name"]
        ret["type"] = info["type"]
        if "environment" in info:
          ret["environment"] = info["environment"]
        if "architecture" in info:
          ret["architecture"] = info["architecture"]
        if obj is object:
          ret["environment"] = environment
          ret["architecture"] = architecture
      else:
        ret["backend"] = obj.__class__.name()

      return ret

    ret = [formatObject(object)]
    ret.extend([formatObject(obj) for obj in path])

    Log.output(json.dumps(ret))
    return 0
