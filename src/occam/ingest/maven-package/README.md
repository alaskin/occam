# Maven Repository Ingest Plugin

This plugin will pull packages from the Maven repository and convert them into
Occam packages. It recursively handles pulling down dependencies and setting up
execution environments that do not require a network for both Maven and SBT
(Scala Build Tool) usage.

There are several goals for this plugin and repository:

* To capture all Maven dependencies without a network.
* To distribute packages by mirroring the Maven repository.
* To build Maven packages as much as possible in the way intended.

There are several issues encountered in mirroring the Maven repository:

* Maven's POM metadata is confusing and at times contradictory.
* Maven requires inheritence of properties from "parent" packages, which have
to be built later.
* Plugins are dependencies used potentially at build time yet do not need to
have a version specified. They pull the latest, which may break things at any
time.

All of these points are circumventable except the last, which the Maven
developers point out is a flaw and they are now requiring versions on all
packages specified in the POM metadata. However, there may be cases where a
package will not build properly.

At any rate, you just aren't supposed to build these things, apparently. They
have repositories instead that MAY work but everybody does things in different
ways. So, really, you can only mirror the repository in a slightly more
proactive way for the sake of preservation. Maybe some day we can have a build
mirror that can build each package.

## What is Maven

Maven is the package manager and repository for Java and JVM-relative languages
such as Scala.

## Maven Package Storage

At a Maven repository server, all packages are stored logically based on their
group and artifact identifiers. The group identifier is the package module and
the artifact identifier is effectively the package's name. Although, packages
also have a human-readable "name" as well.

Based on the group and artifact, the exact location of the package is found
relatively easily. For a package such as
`org.apache.maven.plugins.maven-compiler-plugin`, the artifact identifier is
the last section when you split by a period. In this case,
`maven-compiler-plugin`, with the remaining text being the group. The relative
URL for the package is then `org/apache/maven/plugins/maven-compiler-plugin`.
Yet, there are some cases, such as `javax.inject.javax.inject` where the period
is part of the package name instead. So, in this horrific case, the group is
`javax.inject` and the package is also `javax.inject`. Why.

Here, you are served a directory listing containing all available versions as
separate directories. Within these directories are the package contents. The
main resource found there that must exist is the POM (Project Object Model)
metadata for the package. This is an XML file containing metadata that
describes a variety of features about the package: its name, dependencies,
parent, etc. This file always has the name given by `%artifact%-%version%.pom`.
For our running example, version 3.8.1 would be:
`maven-compiler-plugin-3.8.1.pom`. More on the POM contents as we go.

There are several possible types of resource each package may have alongside
the POM file. The first is a JAR file named `%artifact%-%version%.jar` which
contains the distribution for the package. The next is
`%artifact%-%version%-sources.jar` which contains the source code, if provided.
There may also be a "javadoc" or "test-sources" jar files as well, which
provide documentation or test source code for further use in building.

If a package does not provide a sources file, then it will not need to be built.
In Occam, we will attempt to build any package that has a sources distribution.
Failing that, we will simply distribute the provided distribution instead.

No matter, each resource, including all `.pom` and `.jar` files, will also have
a set of verification files. The associated `.asc` file is a signature, a `md5`
is a MD5 checksum, and a `sha1` is a SHA1 checksum of the files. When Maven
installs a package, it installs the JAR file and the MD5/SHA1 checksums to a
local path, and these must exist (and be correct) for Maven to not seek out the
package on the network. Therefore, Occam will do what it can to ensure these
files are correctly supplied to an environment that depends on them.

## Building a Maven Package

To build a Maven package, you can find the resources that are stored in the
Maven repository. For every version of every package, several files are
produced. For `org.apache.maven.plugins.maven-compiler-plugin` version 3.8.1,
we can find those resources at
[the appropriate directory](https://repo1.maven.org/maven2/org/apache/maven/plugins/maven-compiler-plugin/3.8.1/) on `repo1.maven.org`.

We see a `sources.jar` for this package, so we want to build it ourselves in a
repeatable build. Many packages will build just fine, as long as they do not
need a plugin or two that can't be built or just some strange resources that
are not packaged in the sources distribution. Which is... in a word...
infuriating.

For instance, the `maven-model` package is pretty light. Yet, it needs `modello`
and `modello-maven-plugin` to build. Yet, `modello` needs `maven-model`. Who
made this haunted decision? A side-effect of, perhaps, the lack of strict
versions on plugins.

Furthermore, if you bootstrap `modello-maven-plugin` and attempt to build
`maven-model`, it attempts to find a `maven.mdo` file amongst the sources. It
is not there. It is in the upstream repository on github. So... who even knows.
I suppose the `sources` package in the repository serves *some* purpose, but I
am now sure it is lost knowledge from an ancient civilization who have long
since moved on to much nicer things. I believe it is simply to aid in IDE
lookups and such.

## Mirroring a Maven Package

Occam just has to look up the pertinent information within the POM and capture
the related resources and their checksums as part of the object. It will place
these resources in a `repository` directory followed by a path created from the
group. The `com.github.javaparser.javaparser-core` package will have its JARs
and POM file located in `/repository/com/github/javaparser/javaparser-core`
where those files are in the form of `javaparser-core-3.6.6.pom`, etc.

This corresponds directory to the path expected by Maven and the Coursier cache
it uses. Maven on Occam has been configured to look in `/opt/maven/repository`
for all installed packages. So, each package object will mount its files
directly there such that, in the previous example, you will find the POM file
and associated SHA1 files in
`/opt/maven/repository/com/github/javaparser/javaparser-core/` in which case
Maven will not attempt to download from the network.

## Scala and sbt

The Scala Build Tool (sbt) is the package management tool for Scala. It uses the
Maven repositories and same local cache (Coursier) as Maven. When you parse the
`build.sbt` file for project dependencies, you are looking for a few things. The
file will contain a `scalaVersion` which indicates the version of Scala, and it
will have a `libraryDependencies` variable which is appended for each dependency
the project requires.

The library dependency is a string comprised of three parts: the group,
artifact, and the version. This is simple enough to determine which Maven
package the requirement refers to. However, the usual concatenation operator is
the `%` operator, yet there is a `%%` operator as well. When this is used before
the artifact, it will append an underscore followed by the major and minor
version of Scala indicated in `scalaVersion`.

For example, the following refers to `com.github.javaparser.javaparser-core` at
version 3.6.6:

```
libraryDependencies += "com.github.javaparser" % "javaparser-core" % "3.6.6"
```

And the following refers to `com.typesafe.akka.akka-stream_2.12` at version
2.5.12:

```
scalaVersion := "2.12.6"

libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.5.12"
```

These full names will need to be ingested by the maven plugin, here, and then
carefully used as dependencies in Scala projects on Occam. It is a small wonder
anything gets done, but it must be rather convenient to simply have the package
manager do all the work and assume there is always a working network.
