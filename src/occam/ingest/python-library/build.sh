export CC="gcc"
export CXX="g++"

mkdir -p path
ln -s /usr/bin/gcc path/cc
ln -s /usr/bin/g++ path/cpp
export PATH=$PATH:$PWD/path

mkdir -p /tmp

if [ "$2" == "2" ]; then
  pip2 install "$1" --root="$PWD/../build" -vvv --disable-pip-version-check
else
  pip3 install "$1" --root="$PWD/../build" -vvv --disable-pip-version-check --no-build-isolation
fi
