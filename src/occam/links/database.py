# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql
import sql.aggregate

from occam.log import loggable
from occam.databases.manager import uses, datastore

@loggable
@datastore("links")
class LinkDatabase:
  """ Manages the database interactions for link (object relationship lists) management.
  """

  def queryLinks(self, id=None, sourceObjectId=None, sourceIdentityUri = None, targetObjectId=None, targetObjectRevision=None, relationship=None, order = "descending", key = None):
    """ Returns a instantiated query object for retrieving link records.

    Args:
      id (str): The identifier for the link.
      sourceObjectId (str): The identifier for the source object.
      targetObjectId (str): The identifier for the target object.
      targetObjectRevision (str): The revision for the target object.
      relationship (str): The relationship type.
      key (str): Select the specified key (None means select '*')

    Returns:
      sql.Query: The query to be executed.
    """

    links = sql.Table("links")
    if key:
      query = links.select(links.__getattr__(key))
    else:
      query = links.select()

    query.where = sql.Literal(True)

    if id:
      query.where = query.where & (links.id == id)

    if sourceObjectId:
      query.where = query.where & (links.source_object_id == sourceObjectId)

    if sourceIdentityUri:
      query.where = query.where & (links.source_identity_uri == sourceIdentityUri)

    if targetObjectId:
      query.where = query.where & (links.target_object_id == targetObjectId)

    if targetObjectRevision:
      query.where = query.where & (links.target_object_revision == targetObjectRevision)

    if relationship:
      query.where = query.where & (links.relationship == relationship)

    if order == "ascending":
      query.order_by = sql.Asc(links.published)
    else:
      query.order_by = sql.Desc(links.published)

    return query

  def retrieveLinks(self, id                   = None,
                          sourceObjectId       = None,
                          sourceIdentityUri    = None,
                          targetObjectId       = None,
                          targetObjectRevision = None,
                          relationship         = None):
    """ Returns a set of LinkRecord objects that match the given criteria.

    Args:
      id (str): The identifier for the link.
      sourceObjectId (str): The identifier for the source object.
      targetObjectId (str): The identifier for the target object.
      targetObjectRevision (str): The revision for the target object.
      relationship (str): The relationship type.

    Returns:
      list: A set of LinkRecord objects.
    """

    # One option must be used
    if id is None and sourceObjectId is None and sourceIdentityUri is None and targetObjectId is None and targetObjectRevision is None and relationship is None:
      raise ValueError

    # Create a session
    from occam.links.records.link import LinkRecord
    session = self.database.session()

    # Retrieve the query
    query = self.queryLinks(id, sourceObjectId       = sourceObjectId,
                                sourceIdentityUri    = sourceIdentityUri,
                                targetObjectId       = targetObjectId,
                                targetObjectRevision = targetObjectRevision,
                                relationship         = relationship)

    # Execute the query
    self.database.execute(session, query)

    # Retrieve data
    rows = self.database.many(session, size=100)

    # Return the result
    return [LinkRecord(x) for x in rows]

  def retrieveObjects(self, id                   = None,
                            sourceObjectId       = None,
                            sourceIdentityUri    = None,
                            targetObjectId       = None,
                            targetObjectRevision = None,
                            relationship         = None):
    pass
