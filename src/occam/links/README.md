# The Links Component

This component manages links between objects and local directories.

## Traditional Relationships

To manage lists of objects and ties between them, a `LinkRecord` can be created
for that purpose. The link is a simple record with a source ID (which can be an
object identifier or actor identity), a target ID (again, either object or actor
identity), and a relationship "kind" which is some string that identifies the
type of relationship.

For instance, the bookmarks list consists of all records between the source ID
for an actor's identity of the "bookmark" relationship. The "active"
relationship holds the current objects that are part of a person's collection.
Also, objects can create relationships between them, such as supplemental
material (manuals, documentation, screenshots, etc) when necessary and clients
can then render those lists as they wish.

```python
self.links.create()
```

## Local Links

The LinkManager also keeps track of local instances of Occam objects. Occam can
manage a directory that holds in-progress work in an active object. Such links
are internally called "tracked" links.

Here, a local identifier is used to redirect requests for an object to this
managed directory. The actions (listing, viewing, modifying, etc) made to an
object get, instead, made to this copy. For instance, the following commands
perform actions on a tracked object:

```shell
# View a file in a tracked object
occam objects view QmTUJxV1VKFZzHp4zKCatxvUHZGWa6v9fjafj2atnjC37Y@5dsp2YU6S9KnH96U2gFFjSUgTkAGNg#3/src/main.c

# Update metadata in a tracked object
occam objects set QmTUJxV1VKFZzHp4zKCatxvUHZGWa6v9fjafj2atnjC37Y@5dsp2YU6S9KnH96U2gFFjSUgTkAGNg#3/object.json -i summary "This is my object!"

# Build a tracked object
occam builds new QmTUJxV1VKFZzHp4zKCatxvUHZGWa6v9fjafj2atnjC37Y@5dsp2YU6S9KnH96U2gFFjSUgTkAGNg#3
```

Local objects act very differently than objects that are properly kept in the
object repositories. Specifically, all tasks are generated within their tracked
directory and are cached there. When a build is performed, the build is done
incrementally. That is, it reuses the task and virtual environment from one
invocation of the build to another. This mimics the normal development cycle
of `make` where it only builds the content that changes which allows for a more
convenient development process.

Yet, this runs counter to the goals of software preservation. Therefore, special
consideration is made for tracked objects. They cannot be used in any situation
that requires preservation quality, such as workflows. They can only be ran in
special situations and their output is, itself, not preserved. There are extra
flags for deploying tasks that can clean the environmen to ensure that the
object properly builds and runs from a known state. When the object is
"committed," the object will then have to be built from such a clean state in
order to be used in a public-facing situation, such as a workflow.

## Local Builds

## Local Runs
