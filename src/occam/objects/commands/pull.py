# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.object         import Object
from occam.object_info    import ObjectInfo

from occam.log import Log

from occam.manager import uses

from occam.network.manager        import NetworkManager
from occam.versions.write_manager import VersionWriteManager
from occam.nodes.manager          import NodeManager
from occam.builds.manager         import BuildManager
from occam.links.write_manager    import LinkWriteManager
from occam.backends.manager       import BackendManager
from occam.discover.manager       import DiscoverManager
from occam.objects.write_manager  import ObjectWriteManager
from occam.objects.manager        import ObjectJSONError, ObjectResolveVersionError
from occam.permissions.manager    import PermissionManager
from occam.keys.write_manager     import KeyWriteManager, KeySignatureExistsError

from occam.commands.manager import command, option, argument

@command('objects', 'pull',
  category      = "Object Discovery",
  documentation = "Pulls an object from a specific URL or path.")
@argument("object", 
          action  = "store",
          type    = "object",
          default = ".",
          nargs   = "?")
@option("-f", "--force",
        action = "store_true",
        dest   = "force",
        help   = "pull even if the object is already known")
@option("-a", "--all",
        action = "store_true",
        dest   = "pull_all",
        help   = "will pull each known tag")
@option("-p", "--pull-task",
        action = "store_true",
        dest   = "pull_task",
        help   = "will pull all objects referenced along with the given task")
# TODO: Add option to suppress the pulling of a build binary
@option("--no-build",
        action = "store_true",
        dest   = "no_build",
        help   = "will only pull object metadata and not pull down a built binary.")
@option("-b", "--with-build-dependencies",
        action = "store_true",
        dest   = "with_build_dependencies",
        help   = "will pull down build dependencies as well.")
@option("-m", "--message",
        action = "store",
        dest   = "message",
        help   = "a summary of local changes when pulling a staged object.")
@uses(NetworkManager)
@uses(VersionWriteManager)
@uses(NodeManager)
@uses(LinkWriteManager)
@uses(BuildManager)
@uses(DiscoverManager)
@uses(BackendManager)
@uses(PermissionManager)
@uses(ObjectWriteManager)
@uses(KeyWriteManager)
class PullCommand:
  def pull(self, obj, link=None, suppressOutput=False):
    # Pull any objects determined by other nodes to be necessary to build/run
    # this one
    revision = obj.revision

    # Store the object
    self.objects.write.store(obj, self.person.identity)

    # Restore the revision
    obj.revision = revision

    # Discover the resources
    resources = self.objects.write.pullResources(obj, self.person.identity)

    # Sign the stored object
    try:
      id, signature_digest, signature_type, signature, published = (
        self.keys.write.signObject(obj, self.person.identity)
      )
      self.keys.write.store(
        obj, signature, signature_type, signature_digest, self.person.identity,
        id, published
      )
      Log.write(key="occam.objects.commands.pull.signed", identity=self.person.identity)
    except KeySignatureExistsError as e:
      pass

    # For introducing objects, they are public-by-default
    if self.options.object.id == "." and self.options.object.link is None:
      self.markAsPublic(obj)
      self.announce(obj)

    objectInfo = self.objects.infoFor(obj)

    # If this object is from a local stage, update the record for that stage
    if link:
      # We need to update links
      self.links.write.updateLocalLink(link, obj.root.revision)

    # Output the new information
    ret = {}
    ret["updated"] = []

    for x in (obj.roots or []):
      ret["updated"].append({
        "id": x.id,
        "uid": x.uid,
        "revision": x.revision,
        "position": x.position,
      })

    ret["updated"].append({
      "id": obj.id,
      "uid": obj.uid,
      "revision": obj.revision,
      "position": obj.position,
    })

    if not suppressOutput:
      import json
      Log.output(json.dumps(ret))

    pull_type = "pulled"
    if self.options.object.link:
      pull_type = "changes"

    Log.done(
      key=f"occam.objects.commands.pull.{pull_type}",
      type=objectInfo.get('type'), name=objectInfo.get('name'), id=obj.id
    )
    return 0

  def markAsPublic(self, obj):
    self.permissions.update(id = obj.id, canRead=True, canWrite=False, canClone=True)

  def announce(self, obj):
    objectInfo = self.objects.infoFor(obj)

    # Announce the object to the federation.
    self.discover.announce(
      obj.id, self.objects.idTokenFor(obj, obj.identity or self.person.identity)
    )

    # Announce subObjects to the federation.
    for subObject in objectInfo.get('includes', []):
      subObjectId = self.objects.idFor(
        obj, obj.identity or self.person.identity,
        subObjectType=subObject.get('type'),
        subObjectName=subObject.get('name')
      )
      subObjectToken = self.objects.idTokenFor(
        obj, obj.identity or self.person.identity,
        subObjectType=subObject.get('type'),
        subObjectName=subObject.get('name')
      )
      self.discover.announce(subObjectId, subObjectToken)

    # Announce each viewer
    objectInfo['views'] = objectInfo.get('views', [])

    if not isinstance(objectInfo['views'], list):
      objectInfo['views'] = [objectInfo['views']]

    for viewer in objectInfo.get('views', []):
      self.discover.announceViewer(viewer.get('type'), viewer.get('subtype'))

    for editor in objectInfo.get('edits', []):
      self.discover.announceEditor(editor.get('type'), editor.get('subtype'))

    providesInfo = objectInfo.get('provides', {})
    if not isinstance(providesInfo, list):
      providesInfo = [providesInfo]

    for provideInfo in providesInfo:
      environment = provideInfo.get('environment')
      architecture = provideInfo.get('architecture')

      if environment and architecture:
        # announce a provider
        self.discover.announceProvider(environment, architecture)

  def pull_all(self, ):
    """ Pull each revision found in "$PWD/.occam/tags".
    """
    tagsPath = os.path.join(".occam", "tags")
    tags = {}
    import json
    try:
      with open(tagsPath, "r") as f:
        tags = json.load(f)
    except json.decoder.JSONDecodeError:
      pass

    for tag,revision in tags.items():
      Log.write("Pulling tag %s" % (tag))
      self.options.object.revision = revision
      try:
        taggedObj = self.objects.resolve(
          self.options.object, person = self.person, allowNone = True
        )
      except ObjectResolveVersionError as e:
        obj = None
      except ObjectJSONError as e:
        Log.error("Invalid object.json")
        Log.write(e.report)
        Log.output(e.context, padding="")
        return -1

      self.pull(taggedObj)

      # Look at whether or not the tag is already stored
      versions = self.versions.retrieve(taggedObj)
      versions = list(filter(lambda x: x.tag == tag, versions))
      versions = [version.revision for version in versions]

      if versions:
        if taggedObj.revision not in versions:
          Log.warning("Tag %s already exists." % (tag))
        else:
          Log.warning("The tag %s is already set to this revision." % (tag))
      else:
        # Tag the version in the note store
        signature = None
        published = None
        try:
          signature, published = self.keys.write.signTag(
            taggedObj, self.person.identity, tag
          )
          Log.write("Signed as %s" % (self.person.identity))
        except KeySignatureExistsError as e:
          pass

        if signature and published:
          self.versions.write.update(
            taggedObj, tag, self.person.identity, published, signature
          )

  def findOnFederation(self, obj):
    # Attempt to discover this object and all of it's dependencies.
    Log.write("Cannot find the object locally; trying to discover")
    if self.options.pull_task:
      obj = self.discover.discoverTask(id = self.options.object.id,
                                       revision = self.options.object.revision,
                                       person = self.person)
    else:
      obj = self.discover.discover(id = self.options.object.id,
                                   revision = self.options.object.revision,
                                   version = self.options.object.version,
                                   withBuild = not self.options.no_build,
                                   withBuildDependencies = self.options.with_build_dependencies,
                                   person = self.person)

    if obj:
      self.markAsPublic(obj)
      self.announce(obj)

      Log.done("Object discovered")

    return obj

  def handleLinks(self, obj):
    """ If the object is linked, commit any staged changes and pull all revised
    objects.

    Arguments:
      obj (Object): The object whose links should be handled.

    Returns:
      A reference to the first link found in the database corresponding to the
      given object.
    """
    if not obj.link:
      return None

    # We need a reference to the local link
    links = self.links.retrieveLocalLinks(obj.root, link_id = int(obj.link))

    if not links:
      return None

    link = links[0]

    # We need to commit the staged changes,
    # but we also need to recursively consider all contained objects which
    # also may have changed. The "commitLocal" function does this.
    objects = self.objects.write.commitLocal(obj, self.person.identity, message=self.options.message)

    # Pull all of the revised objects that were changed for the above
    # commit.
    for object in objects[1:]:
      self.pull(object, suppressOutput=True)

    return link

  def findBuildOnFederation(self, obj):
    build = None
    if not self.options.no_build:
      # Ask the federation for a build for this object if one doesn't exist
      # locally.
      if not self.builds.retrieveAll(obj):
        build = self.discover.discoverBuild(
          id = self.options.object.id,
          revision = self.options.object.revision,
          version = self.options.object.version
        )

    if not build:
      Log.done("Object already exists")
    else:
      Log.done("Discovered build")

    self.announce(obj)

    ret = {}
    ret["updated"] = []

    # Report the object status
    ret["updated"].append({
      "id": obj.id,
      "uid": obj.uid,
      "revision": obj.revision,
      "position": obj.position,
    })

    import json
    Log.output(json.dumps(ret))

  def do(self):
    pull_from_dir = (
      self.options.object is None or (self.options.object.id == ".")
    )

    if ((self.person is None or not hasattr(self.person, 'id')) and
        (self.options.object is None or pull_from_dir)):
      Log.error("Must be authenticated to store new objects")
      return -1

    Log.header(key="occam.objects.commands.pull.header")

    # Grab the object from the command line argument
    try:
      # This will be None if the object doesn't exist locally.
      obj = self.objects.resolve(
        self.options.object, person = self.person, allowNone = True
      )
    except ObjectJSONError as e:
      Log.error("Invalid object.json")
      Log.write(e.report)
      Log.output(e.context, padding="")
      return -1

    # Objects being pulled by ID (instead of from the current working
    # directory) can be discovered on the federation if they weren't found
    # locally or if we want to force the pull
    if not pull_from_dir:
      if obj is None or self.options.force:
        obj = self.findOnFederation(obj)
    else:
      if obj:
        # We pulled this object locally, but there may be dependencies we need
        # to grab from the federation.
        self.discover.getObjectPrereqs(None,
                                       self.objects.infoFor(obj),
                                       self.discover.getPrereqSections(withResources = True,
                                                                       withDependencies = True),
                                       self.discover.getActionSections(withBuildDependencies = self.options.with_build_dependencies),
                                       self.person)

    # Local and remote discovery failed.
    if not obj:
      Log.error("Object not found")
      return -1

    # If we found the object locally, and didn't get it from a directory, we
    # want to find the build associated with the object if it exists within the
    # federation.
    if not pull_from_dir and self.options.object.link is None:
      self.findBuildOnFederation(obj)
      return 0

    # Past this point the object was either a linked object or was pulled from
    # a directory.

    # Check for and record local changes.
    link = self.handleLinks(obj)

    if self.options.pull_all:
      self.pull_all()

    return self.pull(obj, link)
