# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.backends.manager import backend

import sys
import codecs
import select
import subprocess
import os
import glob
import time
import json
import shutil
import base64

@backend('singularity')
class Singularity:
  """ Implements the logic to build/run objects as Singularity processes.
  """

  def initialize(self):
    pass

  @staticmethod
  def provides():
    """ Returns a list of all possible environments and architecture pairs that
    can be created and used by this backend.
    """

    return [
      ['singularity', 'x86-64']
    ]

  @staticmethod
  def name():
    return "singularity"

  @staticmethod
  def canProvide(environment, architecture):
    """ Returns True if this backend can run the given environment/architecture.
    """

    return [environment, architecture] in Singularity.provides()

  @staticmethod
  def mountCommandForSoftLink(hostPath, access='ro', mounted=None):
    """
    Returns the list of command tokens (['-v', 'path:path:ro', ...]) to use to
    generate the docker command to mount in the given file represented by path
    and all files it links to.
    """

    mounted = mounted or {}

    command = []
    if os.path.exists(hostPath):
      if not hostPath in mounted:
        command += ['-B', '%s:%s:%s' % (hostPath, hostPath, access)]
        mounted[hostPath] = True

      if os.path.islink(hostPath):
        curPath = os.path.dirname(hostPath)
        nextPath = os.readlink(hostPath)
        nextPath = os.path.join(curPath, nextPath)
        command += Singularity.mountCommandForSoftLink(nextPath, access, mounted)
    return command

  def mountNVIDIADriver(self, mounted=None):
    """
    Returns the list of command tokens (['-v', 'path:path:ro', ...]) to use to
    generate the docker command to mount in the NVIDIA driver if it exists on
    the system.
    """

    # TODO: use ld.so.conf
    SEARCH_LIST=self.occam.system.libraryPaths() + ["/usr/lib/xorg/modules", "/usr/lib/xorg/modules/drivers", "/usr/lib/xorg/modules/extensions", "/usr/lib/x86_64-linux-gnu/vdpau"]
    FILE_LIST=["libnvidia*", "libnvcu*", "libwfb*", "nvidia_drv*", "libglx*"]

    mounted = mounted or {}

    command = []
    command += Singularity.mountCommandForSoftLink("/usr/lib/libGL.so", "ro", mounted)
    command += Singularity.mountCommandForSoftLink("/usr/lib/libGL.so.1", "ro", mounted)

    for pattern in FILE_LIST:
      for root in SEARCH_LIST:
        for path in glob.iglob(os.path.join(root, pattern)):
          command += Singularity.mountCommandForSoftLink(path, "ro", mounted)

    return command

  def mergeVariables(self, a, b):
    if 'LD_LIBRARY_PATH' in a and 'LD_LIBRARY_PATH' in b:
      a['LD_LIBRARY_PATH'] = "%s:%s" % (a['LD_LIBRARY_PATH'], b['LD_LIBRARY_PATH'])
      del b['LD_LIBRARY_PATH']
    a.update(b)
    return a

  # TODO: purgeBuiltImage
  def purge(self, obj):
    """
    Deletes any local cached copy of the built object.
    """

    # TODO: this D: D: D:
    return False

  def detect(self):
    """ Returns True when singularity is available.
    """

    # Just check that singularity is in PATH and runs.
    command = ['singularity']

    # Singularity, by itself, returns 1, for some reason.
    return self.executeSingularityCommand(command) == 1

  def canRun(self, obj):
    """ Returns True when the given object can be run on this backend.
    """

    objInfo = self.objects.infoFor(obj)
    environment  = objInfo.get('environment')
    architecture = objInfo.get('architecture')

    return Singularity.canProvide(environment, architecture)

  # TODO: export
  def export(self, obj):
    """
    """
    return False

  def run(self, task, paths, network, stdout, stdin=None):
    """ Runs the task using a singularity backend.

    Fails if the singularity command or container init fails. Note that this is
    different from the task failing. To determine if the task failed inspect
    the run report. See pullRunReport() in the jobs manager.
    """

    #self.prepare(task, paths)
    #return 0

    if stdout is None:
      import sys
      stdout = sys.stdout.buffer

    # First, ensure that the base container is built
    baseContainer = task['running'][0]['objects'][0]
    baseContainerId = baseContainer.get('id')

    command = ['singularity', '--silent', 'exec']

    # Run as the current user
    #command = command + ['--user', '%s:%s' % (str(os.getuid()), str(os.getgid()))]

    # Singularity Environment
    environment = task.get('running')[0]['objects'][0]

    # Get rid of all env-vars
    command.append('-e')

    # Do not mount from system root
    command.append('-c')
    command.append('-C')

    # Generate a new pid namespace
    command.append('-p')

    # Mount task volume
    command.append('-B')
    command.append('%s:/home/occam:rw' % paths['home'])
    command.append('-B')
    command.append('%s:/home/occam/task:rw' % paths['task'])

    # Create and bind a tmp directory
    # (We cannot mount a tmpfs ourselves, here... singularity mounts one that's
    #  much too small. TODO: update when singularity can handle mount options.)
    # In the future, we would mount the tmpfs (after the root volume binds
    # below), but for now, we just create a 'tmp' directory in the root.
    #command.append('-B')
    #command.append('/tmp:/tmp:rw')
    # Create the directory in the root task
    tmpPath = os.path.join(paths.get('root'), 'tmp')
    if not os.path.exists(tmpPath):
      os.mkdir(tmpPath)

    # Add root volumes
    rootPath = paths.get('root')
    if rootPath:
      for subPath in os.listdir(rootPath):
        command.append('-B')
        command.append('%s:%s:rw' % (os.path.join(rootPath, subPath), os.path.join("/", subPath)))

    # Add network
    #for port in network.get('ports', []):
    #  command.append("-p")
    #  command.append("%s:%s" % (port['port'], port['bind']))

    # Add each volume
    for k, pathInfo in paths.items():
      if isinstance(pathInfo, dict):
        command.append('-B')
        command.append('%s:%s:ro' % (pathInfo.get('path'), pathInfo.get('mount')))

        if 'localPath' in pathInfo:
          command.append('-B')
          command.append('%s:%s:rw' % (pathInfo.get('localPath'), pathInfo.get('localMount')))

        if 'buildPath' in pathInfo:
          command.append('-B')
          command.append('%s:%s:rw' % (pathInfo.get('buildPath'), pathInfo.get('buildMount')))

    # Add some security holes
    command.extend(self.capabilitiesFor(task))
    #command.extend(self.securityOptionsFor(task))

    # Determine the path of the singularity container image
    containerPath = environment.get('file')
    if containerPath is None:
      # We cannot run using an environment without a container image
      return False

    # Get the local path of the container file
    containerLocalPath = paths[environment.get('index')].get('path')
    containerPath = os.path.join(containerLocalPath, containerPath)

    command.append(containerPath)

    runCommand = environment.get('run', {}).get('command')

    if runCommand is None:
      # We cannot run this
      return False

    if not isinstance(runCommand, list):
      runCommand = [runCommand]

    command.extend(runCommand)

    pty = False
    if task.get('interactive', False):
      stdout = None
      pty = True

    exitCode = self.executeSingularityCommand(
      command, cwd=paths['task'], interactive=True, stdout = stdout,
      stdin = stdin, pty = True
    )

    return exitCode

  def safeName(self, name):
    from uuid import uuid5, NAMESPACE_URL
    return str(uuid5(NAMESPACE_URL, "occam://singularity/%s" % (name)))

  def signal(self, task, signal):
    """ Gives the given signal to the given task, if it is running.

    Uses 'docker kill' to send a signal to the docker process.
    """

    name = "occam-%s" % (self.safeName(task.get('id')))

    # The default is KILL
    if signal < 0:
      signal = 9

    # TODO: determine the proper way to interact
    command = ["singularity", "kill", name, "--signal", str(signal)]

    return self.executeSingularityCommand(command)

  def terminate(self, task):
    """ Gives the given signal to the given task, if it is running.

    Uses 'docker kill' to send a SIGKILL (or other signal) to the docker process.
    """

    return self.signal(task, 9)

  def pullExternal(self, source):
    """
    Pulls the container from the given source found on a non-occam network.
    """

    command = ['docker', 'pull', source]
    code = subprocess.Popen(command).communicate()

  def capabilitiesFor(self, obj):
    """ Returns the capability list needed to run the given object.
    """
    ret = []

    # Allows ptrace
    ret += ["--add-caps", "SYS_PTRACE"]

    return ret

  def executeSingularityCommand(self, command, cwd=None, stdout=None, stderr=None, stdin=None, wait=True, interactive=False, pty = False):
    Log.noisy("Invoking native command: %s %s" % ("(interactive)" if interactive else "", ' '.join(command)))

    # Detect if we are to use the vendored singularity
    vendorPath = os.path.realpath(os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", "..", "..", "..", "vendor", "bin", "singularity"))
    if os.path.exists(vendorPath):
      command[0] = vendorPath

    parseToLog = False
    parseTo = None
    if stdout is not None and stdout != subprocess.PIPE and stdout != subprocess.DEVNULL:
      parseTo = stdout
      parseToLog = True
      stdout = subprocess.PIPE
      stderr = subprocess.PIPE

    if not interactive:
      if stdout is None:
        # Capture stdout and replay it on the log (as stderr)
        stdout = subprocess.PIPE
        parseToLog = True

      if stderr is None:
        # Capture stderr and replay it on the log
        stderr = subprocess.PIPE
        parseToLog = True

    host  = None
    guest = None
    old_tty = None

    def update_size(signum, frame):
      # Get the terminal size of the real terminal, set it on the pty
      try:
        buf = array.array('h', [0, 0, 0, 0])
        fcntl.ioctl(PTY.STDOUT_FILENO, termios.TIOCGWINSZ, buf, True)
        fcntl.ioctl(host, termios.TIOCSWINSZ, buf)

        # Pass along signal
        process.send_signal(signal.SIGWINCH)
      except:
        pass

    if pty:
      import array
      import termios
      import pty as PTY
      import tty
      import signal as UNIXSignal
      import fcntl

      # save original tty setting then set it to raw mode
      try:
        old_tty = termios.tcgetattr(sys.stdin)
        if not parseToLog:
          tty.setraw(sys.stdin.fileno())
      except:
        pass

      # open pseudo-terminal to interact with subprocess
      host, guest = PTY.openpty()

      stdout = guest
      stderr = guest
      stdin  = guest

      UNIXSignal.signal(UNIXSignal.SIGWINCH, update_size)

      update_size(UNIXSignal.SIGWINCH, None)

    process = subprocess.Popen(command, cwd=cwd, stdin=stdin, stdout=stdout, stderr=stderr, start_new_session = pty)

    if pty and guest:
      os.close(guest)
    elif not interactive and process.stdin:
      process.stdin.close()

    utfDecoder = codecs.getreader('utf-8')

    totalRead = 0

    if wait:
      if parseToLog or pty:
        log = b""

        closed = []
        readers = []
        if pty:
          readers = [host]
          if not parseToLog:
            readers.append(sys.stdin)
          closed.append(sys.stdin)
        else:
          stdout = process.stdout
          stderr = process.stderr
          readers = [stdout, stderr]

        try:
          while True:
            readable, writable, exceptional = select.select(readers, [], readers)

            numRead = 0

            for reader in readable:
              read = None
              if reader is sys.stdin and host:
                d = os.read(sys.stdin.fileno(), 10240)
                os.write(host, d)
              elif host and reader is host:
                try:
                  read = os.read(host, 10240)
                except OSError as e:
                  pass

                if read:
                  if parseTo is not None:
                    parseTo.write(read)
                    if parseTo is sys.stdout.buffer:
                      parseTo.flush()
                  else:
                    os.write(sys.stdout.fileno(), read)

                if read is None or len(read) == 0:
                  closed.append(reader)
                  readers.remove(reader)
              else:
                read = reader.read(1)
                log += read
                if (read == b"\n"):
                  if parseTo is not None:
                    parseTo.write(log)
                    if parseTo is sys.stdout.buffer:
                      parseTo.flush()
                  else:
                    Log.external(log, 'singularity')
                  log = b""

                numRead += len(read)
                totalRead += len(read)

                if len(read) == 0:
                  closed.append(reader)
                  readers.remove(reader)

            if len(readable) == 0 and process.poll() is not None:
              break

            if numRead == 0 and len(closed) == 2:
              break
        except KeyboardInterrupt:
          return -1

        if len(log) > 0:
          if parseTo is not None:
            parseTo.write(log)
          else:
            Log.external(log, 'singularity')

      process.communicate()

      if pty and old_tty:
        # restore tty settings back
        try:
          termios.tcsetattr(sys.stdin, termios.TCSADRAIN, old_tty)
        except:
          pass

      return process.returncode
    else:
      return process

  def rootBasepath(self):
    """ Returns the root basepath for the singularity filesystem.
    """

    return {
      "separator": "/",
      "mount": "/occam/{{ id }}-{{ revision }}",
      "cwd": "{{ paths.localMount }}/local",
      "taskLocal": "/home/occam/task",
      "local": "/home/occam/task/objects/{{ index }}",
    }

  def prepare(self, task, paths):
    """ Creates any cached images for the given task.
    """

    # Gather the information about the intended singularity base environment
    environmentInfo = task['running'][0]['objects'][0]
    environmentIndex = environmentInfo['index']
    environmentPath = paths[environmentIndex]['path']

    # Specifically, get the real path of the SIF file for that base environment
    occamBaseImagePath = os.path.join(environmentPath, environmentInfo.get('file'))

    # Gather the path of the prepared filesystem
    basePath = paths['home']
    rootPath = paths['root']

    # Generate the singularity definition file
    definition = f"""
Bootstrap: localimage
From: {occamBaseImagePath}

%setup
  cp -r {rootPath}/* ${{SINGULARITY_ROOTFS}}/.
  chmod -R a+w ${{SINGULARITY_ROOTFS}}
    """

    # Create the singularity image using the prepared files and the definition
    definitionPath = os.path.realpath(os.path.join(basePath, "container.def"))
    containerPath = os.path.realpath(os.path.join(basePath, "container.sif"))
    with open(definitionPath, 'w+') as f:
      f.write(definition)

    # We need to invoke our own singularity binary so we can build without root
    command = ['/home/occam/occam/vendor/singularity', 'build', containerPath, definitionPath]
    code = subprocess.Popen(command).communicate()
