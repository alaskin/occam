# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import os

from occam.log    import Log
from occam.object import Object

from occam.commands.manager import command, option, argument

from occam.objects.manager import ObjectManager
from occam.builds.write_manager  import BuildWriteManager

from occam.manager import uses

@command('builds', 'sign',
  category      = 'Build Management',
  documentation = "Signs a build as your account.")
@argument("object", type="object", nargs="?")
@argument("task",   type="object", help="The build task")
@uses(ObjectManager)
@uses(BuildWriteManager)
class BuildsSignCommand:
  def do(self):
    if self.options.object is None:
      obj = self.objects.resolve(SimpleNamespace(id       = "+",
                                                 revision = None,
                                                 version  = None,
                                                 uid      = None,
                                                 path     = None,
                                                 index    = None,
                                                 link     = None,
                                                 identity = None),
                                 person = self.person)
    else:
      obj = self.objects.resolve(self.options.object,
                                 person = self.person)

    if obj is None:
      Log.error("Could not find the object.")
      return -1

    task = self.objects.resolve(self.options.task,
                                person = self.person)

    if task is None:
      Log.error("Could not find the task.")
      return -1

    # Retrieve a build record
    build = self.builds.retrieve(obj, task)

    if build is None:
      Log.error("Could not find a record of this build.")
      return -1

    # Sign the build
    self.builds.write.sign(self.person.identity, obj, task, build)

    # Verify the build
    build = self.builds.retrieve(obj, task, self.person.identity)
    identity         = build.identity_uri
    published        = build.published
    signature        = build.signature
    signature_type   = build.signature_type
    signature_digest = build.signature_digest
    verifyKeyId      = build.verify_key_id
    signed           = build.signed

    # Re-verify the build
    result = self.builds.verify(obj, 
                                task,
                                identity,
                                verifyKeyId,
                                signature,
                                signature_type,
                                signature_digest,
                                published,
                                signed)
    Log.output(json.dumps(result))
