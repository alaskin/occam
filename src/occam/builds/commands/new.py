# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import copy

from occam.log    import Log
from occam.object import Object

from occam.commands.manager import command, option, argument

from occam.objects.manager import ObjectManager
from occam.builds.write_manager  import BuildWriteManager
from occam.manifests.manager import ManifestManager, BuildRequiredError, DependencyUnresolvedError
from occam.jobs.manager      import JobManager
from occam.links.write_manager import LinkWriteManager

from occam.manager import uses

@command('builds', 'new',
  category      = 'Build Management',
  documentation = "Starts a build of the given object.")
@argument("object", type="object", nargs="?")
@option("-n", "--run",       action  = "store_true",
                             dest    = "on_run",
                             help    = "when specified, it builds all runtime dependencies.")
@option("-r", "--recursive", action  = "store_true",
                             dest    = "recursive",
                             help    = "when specified, builds all necessary objects when needed.")
@option("-i", "--interactive", action = "store_true",
                               help   = "runs in interactive mode. no stdout or stderr are generated.",
                               dest   = "interactive")
@option("-b", "--backend",     action = "store",
                               help   = "forces the use of the given backend.",
                               dest   = "backend")
@option("-l", "--lock",        action = "append",
                               nargs  = 2,
                               help   = "locks a requirement to a particular version.",
                               dest   = "locks")
@option("-c", "--command",     action = "store",
                               help   = "overrides the given build command.")
@option("-f", "--from-clean",  dest   = "from_clean",
                               action = "store_true",
                               help   = "will build from a clean environment.")
@uses(ObjectManager)
@uses(BuildWriteManager)
@uses(ManifestManager)
@uses(LinkWriteManager)
@uses(JobManager)
class BuildsNewCommand:
  def build(self, obj, penalties, local, buildStack = []):
    info = self.objects.infoFor(obj)

    locks = {}

    if obj.link:
      if self.options.from_clean:
        self.links.write.cleanLocalLinkBuild(obj)
      buildPath = self.links.write.initializeLocalLinkBuild(obj)

    if self.options.locks:
      for lock in self.options.locks:
        locks[lock[0]] = lock[1]

    task = None
    resolved = False
    while(not resolved):
      try:
        depth = 0
        for buildingObj, buildingInfo in buildStack + [(obj, info,)]:
          Log.write("%sBuilding %s %s %s[@%s]" % (" " * depth, buildingInfo.get('type'), buildingInfo.get('name'), buildingObj.version + " " if buildingObj.version else "", buildingObj.revision))
          depth = depth + 1
        task = self.manifests.build(obj, id = obj.id, revision = obj.revision, local = local, penalties = copy.deepcopy(penalties), backend = self.options.backend, locks = locks, person = self.person)
        resolved = True
      except BuildRequiredError as e:
        if self.options.recursive:
          try:
            penalties[e.objectInfo.get('id')] = penalties.get(e.objectInfo.get('id'), [])
            penalties[e.objectInfo.get('id')].append(e.objectInfo.get('revision'))
            ret = self.build(e.requiredObject, copy.deepcopy(penalties), local = False, buildStack = buildStack + [(obj, info,)])
            if ret != 0:
              return ret
          except DependencyUnresolvedError as e:
            # Cannot resolve a dependency
            # Therefore, we cannot build this object
            # Penalize it
            Log.warning(f"Rerouting build due to recursive requirement on {e.objectInfo.get('type')} {e.objectInfo.get('name')}")
            penalties[e.objectInfo.get('id')] = penalties.get(e.objectInfo.get('id'), [])
            penalties[e.objectInfo.get('id')].append(e.objectInfo.get('revision'))
        else:
          if len(buildStack) == 0:
            raise e
          penalties[e.objectInfo.get('id')] = penalties.get(e.objectInfo.get('id'), [])
          penalties[e.objectInfo.get('id')].append(e.objectInfo.get('revision'))
          task = None

    if task is None:
      Log.error("Could not generate a build task.")
      return -2

    tasks = [task]

    for task in reversed(tasks):
      #if self.options.task_only:
      if False:
        import json

        ret = {
          "id":       task.id,
          "uid":      task.uid,
          "revision": task.revision
        }

        Log.output(json.dumps(ret))
        return 0

      taskInfo = self.objects.infoFor(task)
      originalTaskId = task.id
      taskInfo['id'] = task.id
      cwd = None
      if local:
        cwd = obj.path
      opts = self.jobs.deploy(taskInfo, revision = task.revision, local=local, person = self.person, interactive=self.options.interactive, command = self.options.command, cwd=cwd)
      report = self.jobs.execute(*opts)

      elapsed = report['time']
      buildPath = report.get('paths').get(taskInfo.get('builds').get('index')).get('buildPath')
      buildLogPath = os.path.join(os.path.dirname(report.get('paths').get('task')), 'task', 'objects', str(taskInfo.get('builds').get('index')), 'stdout.0')

      if not 'runReport' in report or report['runReport'].get('status') == 'failed':
        Log.error("Build failed.")
        return -1

      if not local and not self.options.command:
        self.builds.write.store(self.person.identity, obj, task, buildPath, elapsed, buildLogPath)

    return 0

  def do(self):
    if self.person is None:
      Log.error("Must be authenticated to build.")
      return -1

    local = False
    if self.options.object is None or self.options.object.id == '.' or self.options.object.link:
      local = True

    obj = self.objects.resolve(self.options.object,
                               person = self.person)

    if obj is None:
      # Cannot resolve the object
      Log.error("Could not find the object.")
      return -1

    # Get a task
    penalties = {}

    ret = -1

    if self.options.on_run:
      Log.header("Starting build of runtime requirements")

      info = self.objects.infoFor(obj)

      objInfos = info.get('run', {}).get('dependencies', []) + info.get('dependencies', [])
      objs = [self.objects.retrieve(id = info.get('id'), version = info.get('version'), revision = info.get('revision'), person = self.person) for info in objInfos]

      for subobj in objs:
        subInfo = self.objects.infoFor(subobj)
        if not self.builds.retrieveAll(subobj):
          if 'build' in subInfo:
            ret = self.build(subobj, penalties, local=False)
            if ret != 0 and ret != -2:
              return ret
          Log.write("Build not required for %s %s[@%s]: No build information" % (subInfo.get('name'), subobj.version + " " if subobj.version else "", subobj.revision))
        else:
          Log.write("Build already done for %s %s[@%s]" % (subInfo.get('name'), subobj.version + " " if subobj.version else "", subobj.revision))
    else:
      Log.header("Starting build")
      success = False
      lastObj = None
      while not success:
        try:
          ret = self.build(obj, penalties, local)
          success = True
        except DependencyUnresolvedError as e:
          # Try again
          if lastObj == e.objectInfo:
            # Already tried this
            if not self.options.recursive:
              Log.warning(f"Need to provide a build for {e.objectInfo.get('type')} {e.objectInfo.get('name')}")
              Log.error("A dependency also needs to be built first. Use the --recursive flag to build all.")
              return -1

            # Otherwise, bail
            raise e

          lastObj = e.objectInfo
          penalties = {}
        except BuildRequiredError as e:
          Log.warning(f"Need to provide a build for {e.objectInfo.get('type')} {e.objectInfo.get('name')}")
          Log.error("A dependency also needs to be built first. Use the --recursive flag to build all.")
          return -1

    return ret
