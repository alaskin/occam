# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql

from occam.databases.manager import uses, datastore

from occam.keys.database import KeysDatabase

@uses(KeysDatabase)
@datastore("keys.write", reader=KeysDatabase)
class KeysWriteDatabase:
  """ Manages the database interactions for the private aspects of the Key component.
  """

  def createIdentity(self, uri, key, key_type, published):
    """ Creates a new IdentityRecord.

    Args:
      uri (str): The identity URI.
      key (str): The encoded key.
      key_type (str): The type of key.
      published (datetime): The time the key was created.
    """

    from occam.keys.records.identity import IdentityRecord

    session = self.database.session()

    record = IdentityRecord()
    record.uri = uri
    record.key = key
    record.key_type = key_type
    record.published = published

    self.database.update(session, record)
    self.database.commit(session)

    return record

  def queryIdentityKey(self, uri, key = None):
    """ Returns a query that returns the private IdentityRecord for the given Identity.
    """

    identityKeys = sql.Table("identity_keys")

    if key:
      query = identityKeys.select(identityKeys.__getattr__(key))
    else:
      query = identityKeys.select()

    query.where = (identityKeys.uri == uri)
    return query

  def retrieveIdentityKey(self, uri):
    """ Returns the private IdentityRecord for the given Identity.
    """

    from occam.keys.records.identity_key import IdentityKeyRecord

    session = self.database.session()

    query = self.queryIdentityKey(uri)

    self.database.execute(session, query)
    
    row = self.database.fetch(session)
    if row:
      row = IdentityKeyRecord(row)

    return row

  def createIdentityKey(self, uri, key, key_type, verifyKey, published):
    """ Creates a new IdentityKeyRecord.

    Args:
      uri (str): The identity URI.
      key (str): The encoded key.
      key_type (str): The type of key.
      verifyKey (str): The identifier for the current verifyKey.
      published (datetime): The time the key was created.
    """

    from occam.keys.records.identity_key import IdentityKeyRecord

    session = self.database.session()

    record = IdentityKeyRecord()
    record.signing_key_id = verifyKey.id
    record.uri = uri
    record.key = key
    record.key_type = key_type
    record.published = published

    self.database.update(session, record)
    self.database.commit(session)

    return record

  def querySigningKey(self, uri, id):
    """ Returns a query that returns the given private SigningKey for the given Identity.
    """

    signingKeys = sql.Table("signing_keys")

    return signingKeys.select(where = (signingKeys.id == id))

  def retrieveSigningKey(self, uri, id):
    """ Returns the given private SigningKey for the given Identity.
    """

    from occam.keys.records.signing_key import SigningKeyRecord

    session = self.database.session()

    query = self.querySigningKey(uri, id)

    self.database.execute(session, query)
    
    row = self.database.fetch(session)
    if row:
      row = SigningKeyRecord(row)

    return row

  def querySigningKeyFor(self, uri):
    """ Returns the private SigningKey for the given Identity.
    """

    signingKeys = sql.Table("signing_keys")

    subQuery = self.queryIdentityKey(uri, key = "signing_key_id")

    return signingKeys.select(where = (signingKeys.id.in_(subQuery)))

  def retrieveSigningKeyFor(self, uri):
    """ Returns the private SigningKey for the given Identity.
    """

    from occam.keys.records.signing_key import SigningKeyRecord

    session = self.database.session()

    query = self.querySigningKeyFor(uri)

    self.database.execute(session, query)
    
    row = self.database.fetch(session)
    if row:
      row = SigningKeyRecord(row)

    return row

  def createSigningKey(self, key, key_type, verifyKey):
    """ Creates a new SigningKeyRecord with the given PEM and verifyKey.

    Args:
      key: The encoded key.
      key_type: The type of key.
      verifyKey (VerifyKeyRecord): The existing corresponding public verification key.

    Returns:
      SigningKeyRecord: The new record.
    """

    from occam.keys.records.signing_key import SigningKeyRecord

    session = self.database.session()

    record = SigningKeyRecord()
    record.id = verifyKey.id
    record.key = key
    record.key_type = key_type

    self.database.update(session, record)
    self.database.commit(session)

    return record

  def createVerifyKey(self, uri, id, key, key_type, signature, signature_type, signature_digest, published, revokes = None):
    """ Creates a new VerifyKeyRecord with the given PEM and signature.

    Args:
      uri (str): The identity URI.
      id (str): The verify key ID.
      key (str): The encoded key.
      key_type (str): The type of key.
      signature (bytes): The key signed with an Identity.
      signature_type (str): The type of signing algorithm.
      signature_digest (str): The hashing algorithm used on the signing token.
      published (datetime): The time the key was created.
      revokes (id): The old key this one revokes (None, if none).

    Returns:
      VerifyKeyRecord: The new record.
    """

    from occam.keys.records.verify_key import VerifyKeyRecord

    session = self.database.session()

    record = VerifyKeyRecord()
    record.id = id
    record.uri = uri
    record.key = key
    record.key_type = key_type
    record.signature = signature
    record.signature_type = signature_type
    record.signature_digest = signature_digest
    record.revokes = revokes
    record.published = published

    self.database.update(session, record)
    self.database.commit(session)

    return record

  def deleteVerifyKey(self, uri, id):
    """ Deletes a VerifyKeyRecord with the given ids.

    Args:
      uri (str): The identity identifier
      id (str): The verification key identifier
    """

    session = self.database.session()
    row = self.keys.retrieveVerifyKey(uri, id)
    self.database.delete(session, row)
    self.database.commit(session)

    return row

  def createSignature(self, uuid, revision, identity, signature, signature_type, signature_digest, verify_key_id, published):
    """ Creates a new SignatureRecord.

    Args:
      uuid (str): The object id.
      revision (str): The object revision.
      identity (str): The actor's identity URI.
      signature (bytes): The signature raw data.
      signature_type (str): The type of signature.
      signature_digest (str): The hashing algorithm.
      verify_key_id (str): The ID for the verify key to use.
      published (datetime): When the signature was created.
    """

    from occam.keys.records.signature import SignatureRecord

    session = self.database.session()

    record = SignatureRecord()
    record.uid              = uuid
    record.revision         = revision
    record.identity_uri     = identity
    record.signature        = signature
    record.signature_type   = signature_type
    record.signature_digest = signature_digest
    record.published        = published
    record.verify_key_id    = verify_key_id

    self.database.update(session, record)
    self.database.commit(session)

    return record

  def createIdentityVoucher(self, trusteeURI, trustedURI, signature):
    """ Creates an IdentityVoucherRecord.

    There can only be one IdentityVoucherRecord for each pair of trusteeURI
    and trustedURI. If a record is attempted to be stored which is already
    represented in the database by the same trustee and trusted URIs, then this
    existing record is replaced with the one requested by this call.

    Args:
      trusteeURI (str): The Identity URI of the actor creating the trust.
      trustedURI (str): The Identity URI of the trusted party.
      signature (bytes): The signed public key of the trusted party.

    Returns:
      IdentityVoucherRecord: The created record.
    """

    from occam.keys.records.identity_voucher import IdentityVoucherRecord

    session = self.database.session()

    record = IdentityVoucherRecord()

    record.uri                  = trusteeURI
    record.trusted_identity_uri = trustedURI
    record.signature            = signature

    # IdentityVoucherRecords have a unique constraint on their
    # uri/trusted_identity_uri pairs such that if another record is to be
    # written with the same set of URIs, the signature will be overwritten.
    self.database.update(session, record)
    self.database.commit(session)

    return record
