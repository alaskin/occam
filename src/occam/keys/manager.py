# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.log     import loggable
from occam.manager import manager, uses

from Crypto           import Random
from Crypto.PublicKey import RSA
from Crypto.Cipher    import PKCS1_OAEP
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash      import SHA512, SHA384, SHA256, SHA, MD5

from occam.objects.manager import ObjectManager

@loggable
@manager("keys")
@uses(ObjectManager)
class KeyManager:
  """ This OCCAM manager handles signing and encryption keys for accounts.

  When objects are published or tagged, they can be signed. When you request a
  particular version or revision of an object, they can be requested with a
  particular signature. The object can then be retrieved only when verified
  against the signature that represents that identity.

  Servers can also be a signing authority.

  Server private keys verify Account private keys.

  A SignatureRecord holds the signed content of a particular object revision.

  The database records consist of the Identity, which represents an actor within
  the system. The Identity contains a public key. There is an IdentityKey which
  represents the private key. This could be lost (perhaps purposely) and
  recovered by gathering a set of KeyShard records. There is, associated with
  the Identity, a SigningKey (private) which has an associated VerifyKey
  (public).

  The Identity is referred by its URI. Considering Identity is decentralized in
  the best of cases, the URI itself is based on something that does not refer
  directly to the source (such as a domain). Instead, the URI refers to the
  identifying key, which is the private key used to sign the signing keys for
  that identity. The URI is the hash of the public key that corresponds to this
  private key. The URI itself is signed by the private key upon its creation.

  If this private identifying key is lost, then the identity is also lost and
  must be abandoned by its owner. This person must then create a new identity
  and resign their past work under their new identity and, through some social
  mechanism, tell others of this circumstance.
  """

  # The possible keys that can be used to represent an actor.
  KNOWN_KEY_TYPES = ["RSA", "Ed25519"]

  # Known ways the keys can be represented or transported.
  KNOWN_KEY_REPRESENTATIONS = {
    "RSA": ["PEM", "OpenSSH"],
    "Curve25519": ["base64"],
    "Ed25519": ["base64"],
  }

  def newKeyPair(self, type):
    """ Returns a tuple containing new public and private keys respectively.

    Args:
      type (str): The type of key to produce. It can be "RSA" or "Curve25519"

    Returns:
      tuple: (public key, private key), which are both utf-8 strings.
    """

    if type == "RSA":
      random_generator = Random.new().read
      key = RSA.generate(4096, random_generator)
      publicKey  = key.publickey().exportKey('PEM', pkcs=1).decode('utf-8')
      privateKey = key.exportKey('PEM', pkcs=1).decode('utf-8')
    elif type == "Curve25519":
      from nacl.public import PrivateKey
      import base64

      # Create a keypair
      sk = PrivateKey.generate()
      pk = sk.public_key

      # Curve25519 identities are base64 encoded keys
      publicKey = base64.b64encode(bytes(pk)).decode('utf-8')
      privateKey = base64.b64encode(bytes(sk)).decode('utf-8')

    return (type, publicKey, privateKey,)

  def newSigningPair(self, type):
    """ Returns a tuple containing new verify and signing keys respectively.

    Args:
      type (str): The type of key to produce. It can be "RSA" or "Curve25519"

    Returns:
      tuple: (verify key, signing key), which are both utf-8 strings.
    """

    if type == "RSA":
      _, verifyKey, signingKey = self.newKeyPair(type)
    elif type == "Curve25519" or "Ed25519":
      from nacl.signing import SigningKey
      import base64

      # Create a keypair
      sk = SigningKey.generate()
      vk = sk.verify_key

      # Curve25519 identities are base64 encoded keys
      verifyKey = base64.b64encode(bytes(vk)).decode('utf-8')
      signingKey = base64.b64encode(bytes(sk)).decode('utf-8')

    return (type, verifyKey, signingKey,)

  def randomHex(self, length):
    """ Generates a random hexadecimal string of the specified length.

    Args:
      length (int): The number of characters required in the returned string.

    Returns:
      str: A utf-8 encoded string representing a random hexadecimal string.
    """

    import binascii
    return binascii.hexlify(os.urandom(length)).decode('utf-8')

  def importKey(self, key, type):
    """ Imports and returns an instantiation of the key provided by the given string.

    Args:
      key (str): The encoded key.
      type (str): The key type.
    """

    if type == "RSA":
      return RSA.importKey(key)
    elif type == "Curve25519" or type == "Ed25519":
      import base64
      return base64.b64decode(key.encode('utf-8'))

  def exportKey(self, key, type, encoding = None):
    """ Returns a key represented with the given encoding.

    The possible encodings are "OpenSSH", "PEM" for RSA and "base64" for Curve25519.

    Args:
      key (RSAKey or bytes): The imported key.
      type (str): The key type.
      encoding (str): The encoding to return. When None, it uses a default encoding.
      
    Returns:
      str: The encoding encoding used.
      str: The key in the given encoding.
    """

    if encoding is None:
      encoding = KeyManager.KNOWN_KEY_REPRESENTATIONS[type][0]

    ret = None
    if type == "RSA":
      try:
        ret = key.exportKey(encoding, pkcs=1).decode('utf-8')
      except ValueError:
        raise KeyFormatError(encoding)
    elif type == "Curve25519" or type == "Ed25519":
      if encoding == "base64":
        import base64
        ret = base64.b64encode(key).decode('utf-8')

    return encoding, ret

  def retrieve(self, uri):
    """ Retrieves the IdentityRecord for the given identity URI.

    Args:
      uri (str) The identity URI.

    Returns:
      IdentityRecord The corresponding identity record or None if it cannot be found.
    """

    return self.datastore.retrieveIdentity(uri)

  def identityFor(self, uri):
    """ Retrieves the public identifying key for the given Identity URI.

    This key is used for verification of verifying keys (public signing keys)
    for use by the outside world in investigating trust.

    Args:
      uri (str): The identity URI to look up.

    Returns:
      publicKey: The public key for this Person.
    """

    ret = self.retrieve(uri)

    if ret is None:
      raise KeyIdentityUnknownError(uri)

    ret = ret.key
    if not isinstance(ret, str):
      ret = ret.decode('utf-8')

    return ret

  def idFor(self, publicKey, type):
    """ Encodes the given public key. 

    Args:
      publicKey (str): The encoded key.
      type (str): The key type.

    Returns:
      str: The hashed identifier representing the key.
    """

    from occam.storage.plugins.ipfs_vendor.multihash import multihash

    # Create a URI by hashing the public key with a multihash
    if type == "Curve25519":
      import base64
      hashedBytes = multihash.encode(base64.b64decode(publicKey), multihash.ED25519)
    elif type == "Ed25519":
      import base64
      hashedBytes = multihash.encode(base64.b64decode(publicKey), multihash.ED25519)
    else:
      hashedBytes = multihash.encode(publicKey, multihash.SHA2_256)

    from occam.storage.plugins.ipfs_vendor.base58 import b58encode
    uri = b58encode(bytes(hashedBytes))

    return uri

  def verifyingKeysFor(self, uri):
    """ Retrieves the verifying keys for the given Identity URI.

    Args:
      uri (str): The identity URI to look up.

    Raises:
      KeyIdentityUnknownError: No such identity is known.

    Returns:
      list: A list of each VerifyKeyRecord known for this identity.
    """

    ret = self.datastore.retrieveVerifyKeys(uri)

    if not ret:
      raise KeyIdentityUnknownError(uri)

    return ret

  def verifyingKeyFor(self, uri, id):
    """ Retrieves the public signing key for the given Identity URI.

    Arguments:
      uri (str): The Identity URI
      id (str): The identifier for the verify key

    Raises:
      KeyIdentityUnknownError: No such identity is known.

    Returns:
      tuple: (type, key) where type is the key format and key is the public signing key for this Person.
    """

    ret = self.datastore.retrieveVerifyKey(uri, id)

    if ret is None:
      raise KeyIdentityUnknownError(uri)

    ret.key_type = ret.key_type or "RSA"

    return ret

  def keyTokenFor(self, uri, verifyingKey, published):
    """ Yields the signature payload for the given key.

    Args:
      uri (str): The identity URI.
      verifyingKey (str): The public key representation.
      published (datetime): The date when the key was created.

    Returns:
      str: The complete token that is to be signed representing the key.
    """

    return (uri + published.isoformat() + "Z" + verifyingKey).encode('utf-8')

  def verifyKey(self, uri, verifyingKey, published, signature, signature_type, signature_digest, publicKey, type):
    """ Determines if the verifying and public key belong to the same identity.

    Args:
      uri (str): The identity URI.
      verifyingKey (str): The encoded verify key.
      published (datetime): When the verify key was created.
      signature (bytes): The signed verify key.
      publicKey (str): The encoded public key to use for verification.
      type (str): The type of key for the public key.

    Returns:
      bool: True when the verifyingKey matches the publicKey.
    """

    token = self.keyTokenFor(uri, verifyingKey, published)
    return self.verify(token, signature, signature_type, signature_digest, publicKey, type)

  def encrypt(self, message, publicKey, type):
    """ Encrypts the given message using the given public key.

    Args:
      message (bytes): The message to encrypt.
      publicKey (Key): The key to use.
      type (str): The type of key.

    Returns:
      bytes: The encrypted content.
    """

    if type == "RSA":
      cipher = PKCS1_OAEP.new(publicKey)
      return cipher.encrypt(message)
    elif type == "Curve25519" or type == "Ed25519":
      from nacl.public import SealedBox
      box = SealedBox(publicKey)
      return box.encrypt(message)

  def verify(self, message, signature, signature_type, signature_digest, publicKey, type):
    """ Verifies the given message against the signature and given public key.

    Args:
      message (bytes): The content to verify.
      signature (bytes): A previously generated signature.
      signature_type (str): The signing algorithm.
      signature_digest (str): The hashing digest to use.
      publicKey (Key): The public key to use.
      type (str): The public key type.
    
    Returns:
      bool: True if the signature matches the message.
    """

    type = type or "RSA"
    signature_digest = signature_digest or "SHA512"

    digest = None
    if signature_digest == "SHA512":
      if type == "RSA":
        digest = SHA512.new()
        digest.update(message)
      elif type == "Curve25519" or type == "Ed25519":
        # Ed25519 already does SHA512
        pass
      else:
        import hashlib
        digest = hashlib.new('sha512')
        digest.update(message)

    if type == "RSA":
      signer = PKCS1_v1_5.new(publicKey)
      return signer.verify(digest, signature)
    elif type == "Curve25519" or type == "Ed25519":
      from nacl.signing import VerifyKey
      verifyKey = VerifyKey(publicKey)
      try:
        result = verifyKey.verify(message, signature)
        return True
      except:
        return False

  def tokenFor(self, obj, uri, published, tag = None):
    """ Returns the signable token for the given object and identity.
    """
    
    return self.objects.retrieveFileFrom(obj, "object.json").read() \
         + obj.revision.encode('utf-8') \
         + uri.encode('utf-8') \
         + (published.isoformat() + "Z").encode('utf-8') + (tag or "").encode('utf-8')

  def verifyObject(self, obj, signature, signature_type, signature_digest, uri, id, published):
    """ Verifies that the given object was signed.

    Args:
      obj (Object): The object to verify.
      signature (bytes): A previously generated signature.
      signature_type (str): The signature algorithm.
      signature_digest (str): The hashing function used.
      uri (str): The identity to use.
      published (datetime): The time the signature was created.
    
    Returns:
      bool: True if the signature matches the object.
    """

    verifyKey = self.verifyingKeyFor(uri, id)

    publicKey = self.importKey(verifyKey.key, verifyKey.key_type)

    token = self.tokenFor(obj, uri, published)

    return self.verify(token, signature, signature_type, signature_digest, publicKey, verifyKey.key_type)

  def verifyBuild(self, obj, verifyKeyId, signature, signature_type, signature_digest, uri, task, buildHash, published, built):
    """ Verifies that the given build was signed.
    """

    verifyKey = self.verifyingKeyFor(uri, verifyKeyId)

    publicKey = self.importKey(verifyKey.key, verifyKey.key_type)

    tag = task.id + task.uid + task.revision + buildHash + built.isoformat() + "Z"

    token = self.tokenFor(obj, uri, published, tag = tag)

    return self.verify(token, signature, signature_type, signature_digest, publicKey, verifyKey.key_type)

  def verifyTag(self, obj, signature, signature_type, signature_digest, uri, id, tag, published):
    """ Verifies that the given tag was signed.

    Args:
      obj (Object): The object to verify.
      signature (bytes): A previously generated signature.
      signature_type (str): The signature algorithm.
      signature_digest (str): The hashing function used on the token.
      uri (str): The identity to use.
      id (str): The identifier for the verify key.
      tag (str): The tag name.
      published (datetime): The time the signature was created.
    
    Returns:
      bool: True if the signature matches the object.
    """

    verifyKey = self.verifyingKeyFor(uri, id)

    publicKey = self.importKey(verifyKey.key, verifyKey.key_type)

    token = self.tokenFor(obj, uri, published, tag = tag)

    return self.verify(token, signature, signature_type, signature_digest, publicKey, verifyKey.key_type)

  def signaturesFor(self, obj, identity=None):
    """ Gets all signatures for the given object.

    Args:
      obj (Object): The respective Object.
      identity (str): The Identity URI to inquire. Defaults to any attached
                      to the given object.

    Raises:
      KeySignatureNotFoundError: No such signature record could be found.

    Returns:
      list: An array of type SignatureRecord for all existing records of the signing.
    """

    signatures = self.datastore.retrieveSignatures(obj.id, obj.revision, identity or obj.identity)

    if not signatures:
      raise KeySignatureNotFoundError(obj.id, obj.revision, identity or obj.identity)

    return signatures

  def trustDistanceBetween(self, trustee, trusted):
    """ Determines if there is an existing and known trust relationship.

    Args:
      trustee (str): The Identity URI of the acting entity.
      trusted (str): The Identity URI of the actor to gauge the trust.

    Returns:
      float: The score of the relationship as a distance between the trustee and
             trusted. The value is between 0.0 and 1.0. 0 means there is no such
             relationship. 1 is a direct trust.
    """

    # Obviously a person trusts themselves as much as any other person
    if trustee == trusted:
      return 1.0

    # Gets the best known voucher and, with it, a trust score
    voucher = self.datastore.retrieveIdentityRelationship(trustee, trusted)

    ret = 0
    if voucher is not None:
      ret = pow(0.5, voucher.distance - 1)

    return ret

class KeyError(Exception):
  """ Base class for all key/identity errors.
  """

class KeyFormatError(KeyError):
  """ Unknown key format when importing or exporting keys.
  """

  def __init__(self, format):
    self.format = format

  def __str__(self):
    return "Unknown key format '%s'" % (self.format)

class KeyIdentityUnknownError(KeyError):
  """ A requested identity could not be found.
  """

  def __init__(self, uri):
    self.uri = uri

  def __str__(self):
    return "Unknown identity '%s'" % (self.uri)

class KeySignatureNotFoundError(KeyError):
  """ The requested signature was not known.
  """

  def __init__(self, uuid, revision, uri):
    self.uri      = uri
    self.uuid     = uuid
    self.revision = revision

  def __str__(self):
    return "Signature does not exist for '%s@%s' as %s" % (self.uuid, self.revision, self.uri)
