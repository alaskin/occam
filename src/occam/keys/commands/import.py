# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.keys.write_manager import KeyWriteManager
from occam.databases.manager import DataNotUniqueError

from occam.object import Object
from occam.log import Log

import os

@command('keys', 'import',
  category      = 'Identity Management',
  documentation = "Imports an Identity")
@argument("json_input", help = "The json file containing the identity to import.")
@uses(KeyWriteManager)
class KeyImportCommand:
  """ This command will output a JSON representation encompassing an Identity.
  """

  def do(self):
    import base64
    import json
    import datetime

    # Read json

    data = {}
    with open(self.options.json_input, 'r') as f:
      data = json.load(f)

    publicKeyInfo = data['public']['publicKey']
    uri = publicKeyInfo['id']
    publicKey = base64.b64decode(publicKeyInfo['data']).decode('utf-8')

    Log.write(f"Importing {uri}")

    # Store Identity
    try:
      identityRecord = self.keys.write.datastore.write.createIdentity(uri, publicKey)
    except DataNotUniqueError as e:
      Log.warning("Already Known")
      pass

    lastVerifyKey = None

    for i, verifyKeyInfo in enumerate(data['public']['verifyingKeys']):
      verify_key_id = verifyKeyInfo['key']['id']
      Log.write(f"Importing Verify Key {verify_key_id}")
      verifyKey = base64.b64decode(verifyKeyInfo['key']['data']).decode('utf-8')
      signature = base64.b64decode(verifyKeyInfo['signature']['data'])
      published = verifyKeyInfo['key']['published']
      published = datetime.datetime.strptime(published, "%Y-%m-%dT%H:%M:%S.%fZ")

      # Store VerifyKey
      try:
        verifyKeyRecord = self.keys.write.datastore.write.createVerifyKey(uri, verify_key_id, verifyKey, signature, published)
      except DataNotUniqueError as e:
        Log.warning("Already Known")
        continue

      lastVerifyKey = verifyKeyRecord

      # Retrieve signing key
      if 'private' in data:
        signingKey = base64.b64decode(data['private']['signingKeys'][i]['key']['data']).decode('utf-8')
        Log.write(f"Importing Signing Key {verify_key_id}")

        # Store SigningKey
        try:
          signingKeyRecord = self.keys.write.datastore.write.createSigningKey(signingKey, verifyKeyRecord)
        except DataNotUniqueError as e:
          Log.warning("Already Known")
          pass

    if 'private' in data:
      Log.write(f"Importing Private Key {uri}")
      privateKeyInfo = data['private']['privateKey']
      privateKey = base64.b64decode(privateKeyInfo['data']).decode('utf-8')

      # Store Identity Key
      if lastVerifyKey:
        try:
          identityKeyRecord = self.keys.write.datastore.write.createIdentityKey(uri, privateKey, lastVerifyKey)
        except DataNotUniqueError as e:
          Log.warning("Already Known")
          pass

    return 0
