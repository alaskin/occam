# The Git Component

This is an extention component that allows for Git protocol access to object
and resource repositories. In our case, as a preservation platform, it has been
important to specifically address the preservation case. Augmenting the Git
protocol to also provide archival qualities has been somewhat difficult.

## What is Git

[Git](https://git-scm.com/) is a content archive that maintains the history of
changes of files within that archive, referred to as a Git repository. It is
originally designed for a real-time, distributed tool for software developers
to track changes to source code. Such tools are generically referred to as
Distributed Version Control Systems (DVCS). However, it can be used for a
variety of purposes. It is the main mechanism Occam uses for tracking changes to
object metadata.

Git provides a data storage format that will hold generic binary data and
metadata about how that data has changed. It provides tools that can explore
that history and show data as it was at a previous time. It also provides a
protocol that can distribute both data and such metadata from machine to machine
allowing for the duplication and mirroring of repositories. This is a useful
idea in service to software preservation, but not without its critical nuances.

## Git Preservation: Difficulty

The process of archiving Git repositories is a rather complicated one. Git is a
distributed protocol, for one, so it seems very applicable to the idea of being
spread out across multiple machines. Yet, it is designed to be a productivity
tool first and a preservation tool only by convenience.

When you pull down a Git repository, you perform a `clone` which will negotiate
with an external server and pull down the pieces that make up the complete
repository. Each repository is made up of small chunks that more-or-less
correspond to each separate commit that belongs to that repository's history.
However, when you pull, Git, by default, will pull down only the chunks that it
believes are part of that history.

To determine this, Git maps out the history as a graph. Each commit (that is,
change) made to the repository is added to this graph as a child of the last
commit. This produces an ordered graph that shows the progression from oldest
change to newest. When you pull down a repository, only those changes
represented by the graph are also pulled.

However, there are ways of "destroying" history by modifying that graph in a way
that orphans commits (removes them from their parents) detaching them from the
graph. When you delete a commit or merge/squash commits together, you
effectively "rewrite" history. Yet, you may refer to those commits (the prior
history) at some point in the overall archival history. For instance, you may
use a build of some sofware, of which the source code is tracked with Git, and
the source code used to build it is part of a history that has since been
rewritten. It would still be important to perhaps rebuild that software in the
future; therefore it is necessary to keep around the deleted history.

To do this with the way Git is designed to work by default, you would have to
ensure that particular commits always remain in the overall history, regardless
of modifications made to that commit graph. One method of doing this is provided
by Git in the form of "branches", which are simply names you can attach to
particular commits. These branches, like their name implies, graft commits into
the overall graph and ensure they are always referenced. The "name" conveys an
importance that Git respects, even on distribution, ensuring the commit at that
branch (and its parents) makes the trip.

## Git Preservation: Storage

In Occam, all versioned objects are stored as Git repositories. However, there
are also external Git repositories mirrored from Git stores such as GitLab and
GitHub. For Occam-born repositories, we have much leverage in how they are
ultimately stored in order to provide strong preservation. For mirrored Git
repositories, there are several extra issues that must be considered.

These external repositories are managed by developers who, themselves, have
full use of the Git protocol and standard. They can devise any clever trick
they wish in the design of their repository. They can imploy, for instance,
submodules (which are Git repositories within repositories) with the expectation
that all referenced repositories are available at a particular central
location.

When pulling these, the references have to be tagged and rewritten in such a way
they can operate as they are expected, but have such sub-repositories be
themselves mirrored. The way we do this is to pull, recursively, all
repositories and sub-repositories. Then we rewrite the metadata to point the
"submodules" to their local stores. When they are cloned, the metadata is
rewritten to refer to the Occam node's particular copy of the mirror. This
allows clones to be done directly on the Occam node while allowing submodules
to work as expected even if the original servers disappear.

Another issue related to Git preservation is encapsulating modifications from
different actors specifically in reference to access control and forks. First,
recall that an object has both an `id` and a `uid`. The `id` is tied to a
particular actor and is generally what you use in reference to an object one
requests. The `uid`, on the other hand, is a uniform identifier that reflects
the source of the object. When an object is forked (copied) from one actor to
another, the object gets a new `id` and is effectively a "different" object.
However, one can reason that, as a copy, it is mostly the same. It would be
inefficient and unproductive for an archival system to duplicate the entire
history and data. Therefore, all objects are stored using their `uid` with
Git data deduplicated and shared among all instances of that object, which are
now referred to by their individual `id`, which is both uniform and unique.

The mechanism for achieving this is also provided by Git in service to those
central repository houses such as GitLab. The `alternates` feature allows for
a "fallback" repository to be used when names cannot be found in a particular
repository. What we have done is created two Git repositories for each object.
One is for the `id`, which is stored using the identity URI for the actor, and
the other is the generic `uid` repository, which is stored under a `repository`
directory. The generic repository is established as a type of commit pool
containing every commit while the identity repository is intended as a home for
the history itself. When a particular identity repository does not have a commit
it will look at the generic one to fill in the blanks. Commits known to all
forks (copies) of the repository will be visible to all, but stored only once.
All unique commits will only be visible within the particular repository for the
particular actor.

Again, in order to retain commits from potentially erased parts of the history,
branches are also created in the `id` version of the repository when revisions
are referenced.

## Git Preservation: Distribution

In order to increase overall preservation and availability, these repositories
should be propagated and stored on similar servers within other domains,
institutions, and communities. To provide this mirror, the repositories need to
be duplicated with the same level of preservation without leaking private
information about ongoing work. This is a tricky and counter-intuitive problem.

Given the repositories are set up as previously described, you have two points
of entry into the Git history: the `id` repository and the `uid` commit pool.
When you are pulling the history from one machine to another, you are pulling
using an `id`, therefore you are transferring the history from the `id`
repository to that machine's `uid` commit pool, and then creating an empty
`id` repository. In this case, the two machines differ in their storage, but
they share the same functionality. What is not transferred are the commits that
are not reachable from that `id` repository since they may have been created
within a forked object that was not pulled, and perhaps is private. When that
secondary copy is pulled, the `uid` commit pool of the receiving server will be
augmented with those new commits.

The receiving server will also mirror the branches and tags contained within the
repository. Tags are just special cases of branches intended for versioning, but
are otherwise identical with branches. When the receiving server pulls down the
repository, the normal Git protocol will duplicate the list of branches. It will
use them to determine which commits to pull down, and it will pull down all of
those referred by those branches.

On central Git repositories at GitLab, etc, there is a concept of "latest" that
is used when pulling a repository. However, for preservation purposes, this is a
difficult concept to convey. When you pull a Git repository from an Occam
archive, you will specify the commit you want to use as "latest" and the
metadata will be overwritten on-the-fly to ensure the repository looks as though
that particular revision of the repository were "latest." It should be noted
that the branches ensure that, when it is cloned to become a mirror, all commits
are pulled; even those that are newer than the requested commit, ensuring new
content is discovered.

