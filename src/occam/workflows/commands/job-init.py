# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from types import SimpleNamespace

from occam.objects.write_manager  import ObjectWriteManager
from occam.configurations.manager import ConfigurationManager
from occam.workflows.manager      import WorkflowManager
from occam.jobs.manager           import JobManager
from occam.databases.manager      import DatabaseManager

@command('workflows', 'job-init',
  category      = 'Workflow Management',
  documentation = "Respond to a starting job.")
@argument("job_id", type=str, help = "The identifier for the job.")
@uses(ObjectWriteManager)
@uses(DatabaseManager)
@uses(ConfigurationManager)
@uses(WorkflowManager)
@uses(JobManager)
class JobInitCommand:
  def do(self):
    # Get the run
    job = self.jobs.jobFromId(self.options.job_id)
    run = self.workflows.runFor(self.options.job_id)

    # Get the identity of the job actor
    person = self.jobs.personFor(job)

    workflow = self.objects.parseObjectTag(run.workflow_tag)

    # Initialize the jobs in that run
    try:
      workflow = self.objects.resolve(workflow, person = person)
      self.workflows.launch(workflow, run, person=person)
    except:
      # When job-init fails, the job manager is not going to know how to mark
      # the workflow as a whole failed, so we do that here.
      self.workflows.fail(run)
      raise

    return 0
