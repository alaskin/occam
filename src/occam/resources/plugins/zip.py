# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import json
import zipfile

from occam.resources.manager import resource

from occam.log    import loggable

from occam.resources.plugins.file import FileResource

@loggable
@resource('application/zip')
@resource('application/java-archive')
class ZipResource(FileResource):
  """ This is an OCCAM resource engine for handling repeatable file retrieval.
  """

  @staticmethod
  def popen(command, stdout=None, stdin=None, stderr=None, cwd=None, env=None):
    """ Handles the subprocess spawning for any action not understood by the zipfile library.
    """

    import subprocess
    if stdout is None:
      stdout = subprocess.DEVNULL

    if stderr is None:
      stderr = subprocess.DEVNULL

    return subprocess.Popen(command, stdout=stdout, stdin=stdin, stderr=stderr, cwd=cwd, env=env)

  def actions(self, uuid, revision, path, resourceInfo, actions, destination):
    """ Performs any actions that may be required upon installation.

    In this case, the unpack action.
    """

    path = os.path.join(path, "data")

    if os.path.exists(path):
      actions = resourceInfo.get('actions', {})
      if not isinstance(actions, dict):
        actions = {}

      if resourceInfo and "unpack" in actions:
        ZipResource.Log.write("unpacking file")
        unpackPath = actions['unpack']
        unpackPath = os.path.join(destination, unpackPath)

        # Extract the contents:
        # TODO: handle password
        password = None
        zipFile = zipfile.ZipFile(path, "r")
        try:
          zipFile.extractall(unpackPath, pwd = password)
        except NotImplementedError as e:
          os.system('unzip -o "%s" -d "%s"' % (path, unpackPath))

      if resourceInfo and "postUnpack" in actions:
        if actions["postUnpack"] == "delete" and 'to' in resourceInfo:
          # Delete the zip file from its destination
          ZipResource.Log.write("deleting zip file")
          os.unlink(os.path.join(destination, resourceInfo.get('to')))

      return True

    return False

  def _pullEntryInfo(self, zipinfo, subpath=""):
    """ Internal method to form the item entry for a given tarinfo.
    """

    type = "file"
    name = zipinfo.filename[len(subpath):]
    # NOTE: I'm pretty sure this won't work. Some zip files likely do
    #       not have an entry just for the directory.
    #     : However, I've found one... so, so be it
    if zipinfo.filename.endswith('/'):
      type = "tree"
      name = name[:-1]

    import datetime
    import base64

    item = {"name":  name,
            "size":  zipinfo.file_size,
            "crc":   zipinfo.CRC,
            "compressedSize": zipinfo.compress_size,
            "comment": base64.b64encode(zipinfo.comment).decode('utf-8'),
            "type":  type,
            "mtime": datetime.datetime(*zipinfo.date_time).isoformat()}

    return item

  def _generateCache(self, uuid, revision, path):
    """ Generates a directory cache for the zip file, if it doesn't exist.
    """

    cachepath = os.path.join(path, "cache")

    if not os.path.exists(cachepath):
      os.mkdir(cachepath)
    else:
      return

    # Go through each file and create the metadata file for it
    root = {'items': [], 'dirs': {}, 'names': []}

    zippath = os.path.join(path, "data")
    zipFile = zipfile.ZipFile(zippath, "r")

    for info in zipFile.infolist():
      name = info.filename

      # Split into directories
      subpaths = name.split("/")
      filename = subpaths[-1]
      subpaths = subpaths[:-1]

      current = root
      for subpath in subpaths:
        current['dirs'][subpath] = current['dirs'].get(subpath, {'dirs': {}, 'items': [], 'names': []})
        current = current['dirs'][subpath]

      basepath = "/".join(subpaths) + "/"
      if basepath == "/":
        basepath = ""
      current['items'].append(self._pullEntryInfo(info, basepath))
      current['names'].append(filename)

    def generate(data, subpath):
      if not os.path.exists(subpath):
        os.mkdir(subpath)

      # Make sure known directories are included as "items"
      for k, v in data["dirs"].items():
        if k not in data["names"]:
          data["items"].append({
            "name": k,
            "type": "tree",
            "size": 0
          })
          data["names"].append(k)

      cachefile = os.path.join(subpath, "dir.json")
      with open(cachefile, "w+") as f:
        json.dump({"items": data["items"]}, f)

      for k, v in data["dirs"].items():
        # TODO: ensure subpath can't break out of its root path
        generate(v, os.path.join(subpath, k))

    generate(root, cachepath)

  def retrieveDirectory(self, uuid, revision, path, subpath):
    """ Retrieves the directory listing of the given revision.
    """

    if subpath.startswith("/"):
      subpath = subpath[1:]

    if subpath and not subpath.endswith("/"):
      subpath = subpath + "/"

    # Generate directory structure for the entire file, if needed
    self._generateCache(uuid, revision, path)

    path = os.path.join(path, "data")
    zipFile = zipfile.ZipFile(path, "r")

    ret = []

    # Keep track of possible directory names
    directoryNames = set([])

    for info in zipFile.infolist():
      if info.filename.startswith(subpath) and not "/" in info.filename[len(subpath):] and info.filename[len(subpath):]:
        item = self._pullEntryInfo(info, subpath)
        ret.append(item)
        if item["name"] in directoryNames:
          directoryNames.remove(info["name"])
      elif info.filename.startswith(subpath) and info.filename.endswith("/") and info.filename[len(subpath):].count("/") == 1:
        # Capture directory in root with a trailing '/'
        ret.append(self._pullEntryInfo(info, subpath))
      else:
        dirname = os.path.dirname(info.filename)
        if dirname.startswith(subpath) and not "/" in dirname[len(subpath):]:
          directoryNames.add(dirname)

    for path in list(directoryNames):
      ret.append({
        "name":  path[len(subpath):],
        "size":  0,
        "type":  "tree"
      })

    return {"items": ret}

  def retrieveFileStat(self, uuid, revision, path, subpath):
    """ Retrieves the file status information for the resource at the given revision.
    """

    if subpath.startswith("/"):
      subpath = subpath[1:]

    # Generate the directory cache
    self._generateCache(uuid, revision, path)

    # Look for the cache file, if it is there, use it
    cachefile = os.path.join(path, "cache", os.path.dirname(subpath), "dir.json")
    if os.path.exists(cachefile):
      with open(cachefile, "r") as f:
        try:
          direntries = json.load(f)["items"]
          for direntry in direntries:
            if direntry['name'] == os.path.basename(subpath):
              return direntry
        except:
          pass

    path = os.path.join(path, "data")
    zipFile = zipfile.ZipFile(path, "r")

    try:
      info = zipFile.getinfo(subpath)
    except:
      try:
        info = zipFile.getinfo(subpath + "/")
      except:
        return None

    return self._pullEntryInfo(info)

  def retrieveFile(self, uuid, revision, path, subpath, start=0, length=None):
    """ Retrieves the file found within the resource at the given revision.
    """

    path = os.path.join(path, "data")
    zipFile = zipfile.ZipFile(path, "r")

    if subpath.startswith("/"):
      subpath = subpath[1:]

    try:
      info = zipFile.getinfo(subpath)
    except:
      return None

    ret = None

    try:
      ret = zipFile.open(info)
    except NotImplementedError as e:
      import subprocess

      p = ZipResource.popen(['unzip', '-p', path, subpath], stdout=subprocess.PIPE)

      ret = p.stdout

    ret.read(start)
    return ret

  def traversable(self, uid, revision, path):
    """ Returns True since tar files are traversable.
    """

    return True
