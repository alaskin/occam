# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, time

from occam.log import Log

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.manager   import ObjectManager
from occam.jobs.manager      import JobManager
from occam.jobs.daemon       import JobDaemon

@command('jobs', 'daemon',
    category = "Job Management",
    documentation = "Launches a daemon to run jobs")
@option("-b", "--background", dest    = "background",
                              action  = "store_true",
                              help    = "when specified, runs the daemon as a the background task")
@option("-f", "--foreground", dest    = "foreground",
                              action  = "store_true",
                              help    = "when specified, runs the daemon as a the foreground task and does not exit")
@uses(JobManager)
class DaemonCommand:
  """ Views the logs for a given job.
  """

  def do(self):
    Log.header("Starting Job daemon")
    job_daemon = JobDaemon()

    if self.options.background:
      job_daemon.start()
      return 0

    while self.options.foreground:
      job_daemon.idle(stdout=sys.stdout)
      time.sleep(1)

    job_daemon.idle(stdout=sys.stdout)

    return 0
