from occam.databases.manager import up, patch
import occam.workflows.records.run

@patch("runs_and_jobs")
def updateIdentityFromUID(database):
  """ Convert all person_uid fields to identity and preserve all
  other common values from the original tables.
  """
  from occam.databases.manager import table
  from occam.jobs.records.job import JobRecord
  from occam.objects.manager import ObjectManager
  from occam.workflows.records.run import RunRecord
  import sql


  @table('_old_jobs')
  class OldJobRecord:
    schema = {
      "id": {
        "type": "integer",
        "serial": True,
        "primary": True
      },

      "status": {
        "type":    "string",
        "length":  10,
        "values":  ["started", "queued", "finished", "errored"],
        "default": "queued"
      },

      "kind": {
        "type":   "string",
        "length": 10,
        "values": ["build", "run"]
      },

      "priority": {
        "type": "integer",
        "default": 10
      },

      "scheduler": {
        "type": "string",
        "length": 20
      },

      "scheduler_identifier": {
        "type": "string",
        "length": 128
      },

      "path": {
        "type": "string",
        "length": 256
      },

      "person_uid": {
        "type": "string",
        "length": 48
      },

      "task_uid": {
        "type": "string",
        "length": 48
      },

      "task_revision": {
        "type": "string",
        "length": 128
      },

      "task_name": {
        "type": "string",
        "length": 128
      },

      "task_backend": {
        "type": "string",
        "length": 20
      },

      "interactive": {
        "type": "boolean",
        "default": False
      },

      "queue_time": {
        "type": "datetime"
      },

      "start_time": {
        "type": "datetime"
      },

      "finish_time": {
        "type": "datetime"
      },

      "initialize": {
        "type": "string",
        "length": 20
      },

      "initialize_tag": {
        "type": "string",
        "length": 48
      },

      "finalize": {
        "type": "string",
        "length": 20
      },

      "finalize_tag": {
        "type": "string",
        "length": 48
      },
    }

  @table('_old_runs')
  class OldRunRecord:
    schema = {
      "id": {
        "type": "integer",
        "serial": True,
        "primary": True
      },

      "person_uid": {
        "type": "string",
        "length": 46
      },

      "object_uid": {
        "type": "string",
        "length": 46
      },

      "object_tag": {
        "type": "string",
        "length": 256
      },

      "object_name": {
        "type": "string",
        "length": 128
      },

      "workflow_uid": {
        "type": "string",
        "length": 46
      },

      "workflow_tag": {
        "type": "string",
        "length": 256
      },

      "workflow_name": {
        "type": "string",
        "length": 128
      },

      "queue_time": {
        "type": "datetime"
      },

      "elapsed_time": {
        "type": "datetime"
      },

      "finish_time": {
        "type": "datetime"
      },

      "failure_time": {
        "type": "datetime"
      },

      "job_id": {
        "foreign": {
          "table": "jobs",
          "key": "id"
        }
      },

      "lock": {
        "type": "integer"
      }
    }

  # Rename the existing tables. Tables with foreign keys pointing to these
  # tables will still work.
  session = database.session()
  database.renameTable(session, "jobs", "_old_jobs")
  database.renameTable(session, "runs", "_old_runs")
  database.commit(session)

  handledTables = {"jobs": JobRecord.schema}
  database.createTable("jobs", JobRecord, handledTables)
  database.createTable("runs", RunRecord, handledTables)

  record_classes = {
    "jobs": {"new": JobRecord, "old": OldJobRecord},
    "runs": {"new": RunRecord, "old": OldRunRecord},
  }

  for table in record_classes.keys():
    old = sql.Table(f"_old_{table}")
    query = old.select()

    # Fetch the existing records.
    session = database.session()
    database.execute(session, query)

    rows = database.many(session, size=1000)
    objManager = ObjectManager()
    while rows:
      for row in rows:
        old_row = record_classes[table]["old"](row)
        new_row = record_classes[table]["new"]()

        # Keep all of the values from the old table format the same.
        # Note that this also copies the original ID of the row as well, which
        # will prevent breaking foreign key dependencies.
        for k in old_row._data.keys():
          if k in new_row.schema.keys():
            new_row.__setattr__(k, old_row._data[k])

        # Moving forward we expect all runs and jobs to be tied to a
        # identity, not a person_uid.
        #
        # Convert from uid to identity. Due to a partial replacement
        # previously, the person_uid of the jobs table may contain either a uid
        # or an identity. We convert uids we recognize, and keep identities we
        # recognize. If something else is in the field, we lose it.
        if old_row.person_uid and old_row.person_uid[0] == "Q":
          person = objManager.retrieve(old_row.person_uid)
          new_row.identity = person.identity
        elif old_row.person_uid and old_row.person_uid[0] == "6":
          new_row.identity = old_row.person_uid

        # Insert the row. Note we aren't using database.update(). This is
        # because we want to specifically insert to the original id value of
        # the old tables.
        database.insert(session, new_row)

      try:
        rows = database.many(session, size=1000)
      except:
        rows = []

    # Keep the serial ID value up to date, even if there are skips in the original table.
    database.updateSerial(session, f"{table}", "id")

  # Commit the transaction.
  database.commit(session)
  
  print('done')

# We now have to update all of the dependent tables the same way. They have a
# foreign key dependency on the runs table, and the rename operation doesn't
# affect the table ID. Thus we must create copy tables of the dependent tables
# so the foreign key relation is correct.

@patch("dependent_tables")
def recreateDependentTables(database):
  """ Recreate all of the tables that need to point to the new runs and jobs tables.
  """
  import sql

  from occam.jobs.records.job import JobRecord
  from occam.workflows.records.run import RunRecord
  from occam.workflows.records.run_job import RunJobRecord
  from occam.workflows.records.run_output import RunOutputRecord
  from occam.workflows.records.run_output_object import RunOutputObjectRecord

  # Rename all of the tables.
  session = database.session()

  database.renameTable(session, "run_jobs", "_old_run_jobs")
  database.renameTable(session, "run_outputs", "_old_run_outputs")
  database.renameTable(session, "run_output_objects", "_old_run_output_objects")

  database.commit(session)

  handledTables = {
    "runs": RunRecord.schema,
    "jobs": JobRecord.schema,
  }

  # Create the new tables that will point to the new jobs and runs tables.
  database.createTable("run_jobs", RunJobRecord, handledTables)
  database.createTable("run_outputs", RunOutputRecord, handledTables)
  database.createTable("run_output_objects", RunOutputObjectRecord, handledTables)

  session = database.session()

  record_classes = {
    "run_jobs": RunJobRecord,
    "run_outputs": RunOutputRecord,
    "run_output_objects": RunOutputObjectRecord,
  }

  for t in record_classes.keys():
    old = sql.Table(f"_old_{t}")
    query = old.select()

    # Fetch the existing records.
    database.execute(session, query)

    # Insert into the updated table.
    rows = database.many(session, size=1000)
    while rows:
      for row in rows:
        old_row = record_classes[t](row)
        new_row = record_classes[t]()

        # Keep all of the values from the old table format the same.
        # Note that this also copies the original ID of the row as well, which
        # will prevent breaking foreign key dependencies.
        for k in old_row._data.keys():
          if k in new_row.schema.keys():
            new_row.__setattr__(k, old_row._data[k])

        # Insert the row.
        database.insert(session, new_row)

      try:
        rows = database.many(session, size=1000)
      except:
        rows = []

  database.commit(session)
